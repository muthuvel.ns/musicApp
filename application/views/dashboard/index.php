<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="form-style-2">
   <div class="form-style-2-heading">Dashboard</div>
      <label class="searchlabal" for="field2"><span>Total Users </span><span><?= (!empty($user['USER_COUNT'])?$user['USER_COUNT']:0);?></span></label>
      <label class="searchlabal" for="field1"><span>Total Album </span><span><?= (!empty($album['ALBUM_COUNT'])?$album['ALBUM_COUNT']:0);?></span></label>
      <label class="searchlabal" for="field2"><span>Total Artist </span><span><?= (!empty($artist['ARTIST_COUNT'])?$artist['ARTIST_COUNT']:0);?></span></label>
      <label class="searchlabal" for="field1"><span>Total Songs </span><span><?= (!empty($songs['SONGS_COUNT'])?$songs['SONGS_COUNT']:0);?></span></label>
      <label class="searchlabal" for="field1"><span>Total Subscriptions </span><span><?= '10';?></span></label>
      <label class="searchlabal" for="field2"><span>Amount Settled to Artist </span><span><?= '1000';?></span></label>
      
</div>
<?php /*?>
	<div class="table" >
	<div class="form-style-2-heading">Most Played songs</div>
	<?php if( !empty( $recent ) ) {?>
		<div class="heading"> 
			<div class="coumn1">Album Name</div>
			<div class="coumn2">Song Name</div>
			<div class="coumn3">Artist </div>
			<div class="coumn4">Count</div>
		</div>
		<?php 
				foreach ($recent as $value ){ ?>
		<div class="child">  
			<div class="coumn1"><?= (!empty($value['ALBUM_NAME'])?$value['ALBUM_NAME']:'---'); ?></div>
			<div class="coumn2"><?= (!empty($value['SONG_NAME'])?$value['SONG_NAME']:'---'); ?></div>
			<div class="coumn3"><?= (!empty($value['ARTISTS_USERNAME'])?$value['ARTISTS_USERNAME']:'---'); ?></div>
			<div class="coumn4"><?= (!empty($value['COUNT'])?$value['COUNT']:'---'); ?></div>
		</div>
		<?php 	}	?>	
	</div>
	<?php } else {
				echo '<div class="child"> <h3 style="text-align:center;">No Data Fount</h3> </div>';
		}*/?>
<div class="table">
	<table id="example" class="example" cellspacing="1" border="1" width="100%">
	        <thead class="heading-table">
	            <tr>
	                <th>Album Name</th>
	                <th>Song Name</th>
	                <th>Artist</th>
	                <th>Count</th>
	            </tr>
	        </thead>
          <tbody class="body-table">
         </tbody>
	    </table>
</div>	
		
<script type="text/javascript">
/** menu active script **/
$('#dashboard').addClass('open');

$(window).load(function() {
$('.example').DataTable( {
	"order": [[ 0, "asc" ]],
	destroy: true,
	"searching": false,
	"lengthMenu": [[10, 50, 100], [10, 50, 100]],
    "processing": true,
    "serverSide": true,
    "ajax": $.fn.dataTable.pipeline( {
        url: baseurl+'dashboard/index/search',
        dataType: "json",
//         data: { data: str },
       // dataSrc: "tableData",
        pages: 100 // number of pages to cache
    } )
} );
});
</script>