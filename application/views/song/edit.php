<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="form-style-2">
<div class="form-style-2-heading">Song Management > Update song</div>
   <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i> <?php echo $flash['message']; ?>
		</span>
<?php }
		if ( !empty( $songsData) ){
?>
<form class="innerform" action="<?= base_url().'song/index/edit/'.$songsData['SONG_ID'];?>" method="post" id="songForm" enctype="multipart/form-data">
      <label for="field1"><span>Song Name <span class="required">*</span></span>
      	<input type="text" class="input-field" id="song_name" name="song_name"  value="<?= $songsData['SONG_NAME'];?>" tabindex="1" required />
      	<input type="hidden" name="old_name" value="<?= $songsData['SONG_NAME']?>">
      	<label id="song_name-error" class="error error-msg" for="song_name"></label>
      </label>
      
      <!--<label for="field2" style="padding-bottom: 10px;margin-bottom: 30px;"><span>Cover Image </span>
		  <span style="float:left;"><img alt="LOGO" width="75" height="75" src="<? //= base_url().'assets/upload_images/song/'.$songsData['SONG_COVER_IMAGE']?>" id="blah"></span>
		  <input type="file"  id="logo" name="logo" tabindex="2" onchange="readURL(this);" class="upload" style="margin-top: 30px;"/>
		  <input type="hidden" name="old_logo" value="<? //= $songsData['SONG_COVER_IMAGE']?>">
		  <label id="logo-error" class="error error-msg" for="logo"></label>
	  </label>-->
	  
	  <label for="field2" style="padding-bottom: 10px;margin-bottom: 30px;"><span>Song Audio </span>
		  <input type="file"  id="song" name="song" tabindex="2" onchange="readSongURL(this);" class="upload"/>
		  <p style="margin-left: 111px;" id="audio"><?= $songsData['SONG_URL']?></p>
		  <input type="hidden" name="old_song" value="<?= $songsData['SONG_URL']?>">
		  <label id="song-error" class="error error-msg" for="song"></label>
	  </label>
      
      <label for="field4">
         <span>Album Name<span class="required">*</span></span>
          <?= form_dropdown('albumId', (!empty($albumList)?$albumList:''), $songsData['SONG_ALBUM_ID'], 'class="select-field" id="albumId" tabindex="3" required');?>
         <!-- <select name="artistId" class="select-field" tabindex="3">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select> -->
         <input type="hidden" name="old_albumId" value="<?= $songsData['SONG_ALBUM_ID']?>">
         <label id="albumId-error" class="error error-msg" for="albumId"></label>
      </label>
      <label for="field4">
         <span>Genres Type <span class="required">*</span></span>
         <?= form_dropdown('genresType', $genresType, $songsData['SONG_GENRES_ID'], ' class="select-field" id="genresType" tabindex="4" required');?>
         
        <!-- <select name="genresType" class="select-field" tabindex="4">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <input type="hidden" name="old_genresType" value="<?= $songsData['SONG_GENRES_ID']?>">
         <label id="genresType-error" class="error error-msg" for="genresType"></label>
      </label>
      <label for="field4">
         <span>Mood Type</span>
         <?= form_dropdown('moodType', $moodType, $songsData['SONG_MOOD_ID'], ' class="select-field" id="moodType" tabindex="5"');?>
         <!-- <select name="moodId" class="select-field" tabindex="5">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <input type="hidden" name="old_moodType" value="<?= $songsData['SONG_MOOD_ID']?>">
      </label>
      <label for="field1"><span>Song Amount </span>
      	<input type="text" class="input-field" id="song_amt" name="song_amt"  value="<?= $songsData['SONG_AMOUNT'];?>" tabindex="6" />
      	<input type="hidden" name="old_amt" value="<?= $songsData['SONG_AMOUNT']?>">
      </label>
      <label for="field1"><span>Company Percentage</span>
      	<input type="text" class="input-field" id="percentage" name="percentage"  value="<?= $songsData['SONG_COMPANY_PERCENTAGE'];?>" tabindex="7" />
      	<input type="hidden" name="old_percentage" value="<?= $songsData['SONG_COMPANY_PERCENTAGE']?>">
      </label>
      <label for="field2"><span>Location </span>
      	<input type="text" class="input-field" name="location" tabindex="8" value="<?= $songsData['SONG_LOCATION'];?>" />
      	<input type="hidden" name="old_location" value="<?= $songsData['SONG_LOCATION']?>">
      </label>
     <label for="field2"><span>Country </span>
     	<input type="text" class="input-field" name="country" tabindex="9" value="<?= $songsData['SONG_COUNTRY'];?>">
     	<input type="hidden" name="old_country" value="<?= $songsData['SONG_COUNTRY']?>">
     </label>
     <label for="field2"><span>City </span>
     	<input type="text" class="input-field" name="city" tabindex="10" value="<?= $songsData['SONG_CITY'];?>"/>
     	<input type="hidden" name="old_city" value="<?= $songsData['SONG_CITY']?>">
     </label>
      <label for="field4">
         <span>Song Status</span>
         <select name="song_status" class="select-field" tabindex="11">
            <option value="1" <?php echo (!empty($songsData['SONG_STATUS'])?'selected="selected"':'')?>>Active</option>
            <option value="0" <?php echo (empty($songsData['SONG_STATUS'])?'selected="selected"':'')?>>Deactive</option>
         </select>
         <input type="hidden" name="old_status" value="<?= $songsData['SONG_STATUS']?>">
      </label>
     <label for="field1"><span>Podcast </span>
      <input type="checkbox" class="radio-field" name="podcast"  id="podcast" tabindex="12" <?= (!empty($songsData['SONG_IS_PODCAST']) ? 'checked="checked"' :'');?>/>
      <?= form_dropdown('podcastType', $PodcastType, $songsData['SONG_PODCAST_CATEGORY'], ' class="select-field" id="podcastType" style="display:none;" tabindex="11" required');?>
      <label id="podcastType-error" class="error error-msg" for="podcastType"></label>
     </label>
      
      
       <?php if (!empty( $artistType)){?>
	  <table style="width:50%;text-align: justify;margin-left: 25px;margin-bottom: 10px;" class="">
	  
	  <tr>
	    <th>Artist Type</th>
	    <th>Artist Name</th>
	    <th>Percentage</th>
	  </tr>
	<?php foreach ( $artistType as $key=>$val ) { ?>
	  <tr>
	    <td>
		    <select name="artistType[<?= $key;?>]" class="" onchange="getArtist(this.value,'<?= $key;?>')" id="artistType_<?= $key;?>">
		    	<option value="">Select type</option>
		    	<option value="<?= $key;?>"><?= $val;?></option>
		    </select>
	    </td>
	    <td>
	    	<?php if (!empty($artistList)) {?>
			<select name="artistList[<?= $key;?>]" class="" id="artistList_<?= $key;?>" style="display: none" required>
				<option value="">Select name</option>
				<?php foreach ( $artistList as $art){ 
						if($art['ARTISTS_TYPE_ID'] == $key){
				?>
		    	<option value="<?= $art['ARTISTS_ID'] ?>"><?= $art['ARTISTS_USERNAME'] ?></option>
		    	<?php  } }?>
		    </select>
		    <?php } ?>
	    </td>
	    <td><input type="number" class="" name="percenteage[<?= $key;?>]" id="percenteage_<?= $key;?>" style="display: none" required/></td>
	  </tr>
	  <?php }?>
	</table> 
	 <?php }
   ?>
      <label><span>&nbsp;</span><input type="submit" value="Update" /></label>
   </form>
   <?php } else{  echo '<h3 style="text-align: center">Information Not Found</h3>'; }?>
</div>

<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   
<script type="text/javascript">
/** menu active script **/
$('#song_manage').addClass('open');
$('#song_manage .submenu').show();
$('#song_manage #view').addClass('submenu-color');

$("#song_amt").keypress(function (e) { //digits only allowed
	if (e.which != 8 && e.which != 0 && e.which != 46 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});
$("#percentage").keypress(function (e) { //digits only allowed
	console.log(e.which)
	if (e.which != 8 && e.which != 0 && e.which != 46 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});
$( document ).ready(function() {
	<?php if (!empty($songsPerData)){
		foreach ($songsPerData as $perType){
		?>
			$('#artistType_'+<?= $perType['ARTIST_TYPE_ID']?>).val('<?= $perType['ARTIST_TYPE_ID']?>');
			$('#artistList_'+<?= $perType['ARTIST_TYPE_ID'];?>).val('<?= $perType['ARTIST_ID'];?>').show();
			$('#percenteage_'+<?= $perType['ARTIST_TYPE_ID']?>).val('<?= $perType['ARTIST_PERCENTAGE']?>').show();
	<?php }}?>
});
/*function readURL(input) {
	var ext = $('#logo').val().split('.').pop().toLowerCase(); 	
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) { 		
		$('#logo').val(''); 		 
		alert('Invalid File Format.Allows Only Image File '); 	    
		return false; 	
	} 
	
	if (input.files && input.files[0]) {        
		var reader = new FileReader();         
		reader.onload = function (e) {         	
		$('#blah').attr('src', e.target.result)             
		.width(75)             
		.height(75);         
		}         
		reader.readAsDataURL(input.files[0]);     
	} 
}*/

function readSongURL(input) {
	   var ext = $('#song').val().split('.').pop().toLowerCase();  
   if($.inArray(ext, ["mp3", "ogg", "flac"]) == -1) {      
	      $('#song').val('');      
	      alert('Invalid File Format.Allows Only mp3,ogg and flac File ');
	      $('#audio').show();        
	      return false;  
	   } 

   if( input.files[0].size > 5242880){
	   $('#song').val('');      
	     alert('upload size 5MB only');
	     $('#audio').show();        
	  return false;  
   }

   $('#audio').hide();
   
}

function getArtist(type, value){
	if(type==''||type==0){
		$("#artistList_"+value).hide();
	 	$("#percenteage_"+value).hide();
		return false;
	}
	$.ajax({
		  method: "GET",
		  url: baseurl+"song/index/getartist/"+type,
		  dataType: "html",
		  success: function(result){
			  if(result!="" && result!=0){
			 	$("#artistList_"+type).html(result);
			 	$("#artistList_"+type).show();
			 	$("#percenteage_"+type).show();
			  }
		 }
	});
	
}

$("#song_name").keypress(function (e) { //letters and digits only allowed
   if (e.which != 8 && e.which != 0 &&  e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
      return false;             
   }         
});
$(function () {
   $('input').blur(function () {                        
      $(this).val(
         $.trim($(this).val())
      );
   });

   /** podcast is checked then podcastType is showed*/
	if ($('#podcast').is(':checked')) {
		   $('#podcastType').show();
		  
	  } else {
		  $('#podcastType').hide();
	  }

	$("#songForm").validate({
       rules: {
            song_name: "required",
  //          logo: "required",
            albumId: "required",
            genresType: "required",
         podcastType: "required"
       },
       messages: {
         song_name: "Please enter your song name",
//         logo: "Please add a  cover image",
         albumId: "Please select album name",
         genresType: "Please select genres type",
         podcastType: "Please select podcast type"
       },
       submitHandler: function(form) {
         form.submit();
       }
     });
});

$('#podcast').change(function() {
     if ($(this).is(':checked')) {
      $('#podcastType').show();
     
     } else {
        $('#podcastType').hide();
     }
});

$('#song_name').change(function() {
   $.trim($(this).val());
   return true;
});

</script>