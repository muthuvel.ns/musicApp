<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="form-style-2">
<div class="form-style-2-heading">Song Management > Create song</div>
   <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i><?php echo $flash['message']; ?>
		</span>
<?php }?>
<form class="innerform" action="<?= base_url().'song/index/add';?>" method="post" id="songForm" enctype="multipart/form-data">
      <label for="field1"><span>Song Name <span class="required">*</span></span>
      	<input type="text" class="input-field" id="song_name" name="song_name"  tabindex="1" required />
      	<label id="song_name-error" class="error error-msg" for="song_name"></label>
      </label>
    <!-- <label for="field2"><span>Cover Image <span class="required">*</span></span>
      	<input type="file" class="input-field" name="logo" id="logo" onchange="readURL(this);" tabindex="2" required/>
      	<label id="logo-error" class="error error-msg" for="logo"></label>
      </label>-->
      <label for="field2"><span>Song Audio <span class="required">*</span></span>
      	<input type="file" class="input-field" name="song" id="song" onchange="readSongURL(this);" tabindex="3" required/>
      	<label id="song-error" class="error error-msg" for="song"></label>
      </label>
      <label for="field4">
         <span>Album Name<span class="required">*</span></span>
          <?= form_dropdown('albumId', (!empty($albumList)?$albumList:''), '', 'class="select-field" id="albumId" tabindex="4" required');?>
         <!-- <select name="artistId" class="select-field" tabindex="3">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select> -->
         <label id="albumId-error" class="error error-msg" for="albumId"></label>
      </label>
      <label for="field4">
         <span>Genres Type <span class="required">*</span></span>
         <?= form_dropdown('genresType', $genresType, '', ' class="select-field" id="genresType" tabindex="5" required');?>
         
        <!-- <select name="genresType" class="select-field" tabindex="4">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <label id="genresType-error" class="error error-msg" for="genresType"></label>
      </label>
      <label for="field4">
         <span>Mood Type</span>
         <?= form_dropdown('moodType', $moodType, '', ' class="select-field" id="moodType" tabindex="6"');?>
         <!-- <select name="moodId" class="select-field" tabindex="5">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
      </label>
      <label for="field1"><span>Song Amount </span><input type="text" class="input-field" id="song_amt" name="song_amt"  tabindex="7" /></label>
      <label for="field1"><span>Company Percentage</span><input type="text" class="input-field" id="percentage" name="percentage"  tabindex="8" /></label>
      <label for="field2"><span>Location </span><input type="text" class="input-field" name="location" tabindex="9"/></label>
     <label for="field2"><span>Country </span><input type="text" class="input-field" name="country" tabindex="10"/></label>
     <label for="field2"><span>City </span><input type="text" class="input-field" name="city" tabindex="11"/></label>
      <label for="field4">
         <span>Song Status</span>
         <select name="song_status" class="select-field" tabindex="12">
            <option value="1" selected="selected">Active</option>
            <option value="0">Deactive</option>
         </select>
      </label>
     <label for="field1"><span>Podcast </span>
      <input type="checkbox" class="radio-field" name="podcast"  id="podcast" tabindex="13" />
      <?= form_dropdown('podcastType', $PodcastType, '', ' class="select-field" id="podcastType" style="display:none;" tabindex="14" required');?>
      <label id="podcastType-error" class="error error-msg" for="podcastType"></label>
      
     </label>
      
      
      <?php if (!empty( $artistType)){?>
	  <table style="width:50%;text-align: justify;margin-left: 25px;margin-bottom: 10px;" class="">
	  
	  <tr>
	    <th>Artist Type</th>
	    <th>Artist Name</th>
	    <th>Percentage</th>
	  </tr>
	<?php foreach ( $artistType as $key=>$val ) { ?>
	  <tr>
	    <td>
		    <select name="artistType[<?= $key;?>]" id="artistType_<?= $key;?>"  class="" onchange="getArtist(this.value,'<?= $key;?>')">
		    	<option value="">Select type</option>
		    	<option value="<?= $key;?>"><?= $val;?></option>
		    </select>
	    </td>
	    <td>
			<select name="artistList[<?= $key;?>]" class="" id="artistList_<?= $key;?>" style="display: none" required>
		    	<option value="">Select name</option>
		    </select>
	    </td>
	    <td><input type="number" class="" name="percenteage[<?= $key;?>]" id="percenteage_<?= $key;?>" style="display: none" required/></td>
	  </tr>
	  <?php }?>
	</table> 
	 <?php }
   ?>

      <label class="submit-btn"><span>&nbsp;</span><input type="submit" value="Create" /></label>
   </form>
</div>

<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   
<script type="text/javascript">
/** menu active script **/
$('#song_manage').addClass('open');
$('#song_manage .submenu').show();
$('#song_manage #create').addClass('submenu-color');

$("#song_amt").keypress(function (e) { //song amount digits only allowed
	if (e.which != 8 && e.which != 0 && e.which != 46 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});
$("#percentage").keypress(function (e) { //song percentage digits only allowed
	console.log(e.which)
	if (e.which != 8 && e.which != 0 && e.which != 46 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});

function getArtist(type, value){
	if(type==''||type==0){
		$("#artistList_"+value).hide();
	 	$("#percenteage_"+value).hide();
		return false;
	}
	$.ajax({
		  method: "GET",
		  url: baseurl+"song/index/getartist/"+type,
		  dataType: "html",
		  success: function(result){
			  if(result!="" && result!=0){
			 	$("#artistList_"+type).html(result);
			 	$("#artistList_"+type).show();
			 	$("#percenteage_"+type).show();
			  }else{
				  alert('Artist not found');
				  $("#artistType_"+type).val('');
			  }
		 }
	});
	
}
function readURL(input) {
   var ext = $('#logo').val().split('.').pop().toLowerCase();  
      if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {      
      $('#logo').val('');      
      alert('Invalid File Format.Allows Only Image File ');        
      return false;  
   } 
      if( input.files[0].size > 5242880){
		   $('#logo').val('');      
		     alert('upload size 5MB only');        
		  return false;  
	   }
}

function readSongURL(input) {
	   var ext = $('#song').val().split('.').pop().toLowerCase();  
      if($.inArray(ext, ["mp3", "ogg", "flac"]) == -1) {      
	      $('#song').val('');      
	      alert('Invalid File Format.Allows Only mp3,ogg and flac File ');        
	      return false;  
	   } 

	   if( input.files[0].size > 5242880){
		   $('#song').val('');      
		     alert('upload size 5MB only');        
		  return false;  
	   }
      
}

$("#song_name").keypress(function (e) { //letters and digits only allowed
   if (e.which != 8 && e.which != 0 &&  e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
      return false;             
   }         
});
$(function () {
   $('input').blur(function () {                        
      $(this).val(
         $.trim($(this).val())
      );
   });

	$("#songForm").validate({
       rules: {
            song_name: "required",
            logo: "required",
            albumId: "required",
            genresType: "required",
         podcastType: "required"
       },
       messages: {
         song_name: "Please enter your song name",
         logo: "Please add a  cover image",
         albumId: "Please select album name",
         genresType: "Please select genres type",
         podcastType: "Please select podcast type"
       },
       submitHandler: function(form) {
         form.submit();
       }
     });
});

$('#podcast').change(function() {
     if ($(this).is(':checked')) {
      $('#podcastType').show();
     
     } else {
        $('#podcastType').hide();
     }
});

$('#song_name').change(function() {
   $.trim($(this).val());
   return true;
});

</script>