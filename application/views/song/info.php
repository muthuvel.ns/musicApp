<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading">Song Management > View Song
   
    <?php if (!empty( $songData['SONG_ID'])){?>
	   <a style="float: right; text-decoration: none;" href="<?= base_url().'song/index/edit_songs/'.$songData['SONG_ID'];?>">Edit</a> 
	   <?php } ?>
   </div>
<?php 
		if (!empty( $songData )){//echo '<pre>';print_r($songData);exit;
?>
		<label for="field2" style="margin-left: 15px;">
		  <span ><img alt="IMAGE" width="75" height="75" src="<?= base_url().'assets/upload_images/song/'.$songData['SONG_COVER_IMAGE'];?>" id="blah"></span>
	  	</label>
	  	<div style="margin: 20px 0px 60px 120px;">
	     <label class="informationdiv">
			<span class="textleft">Album Name</span>
			<span class="textright"><?= (!empty($songData['ALBUM_NAME'])?$songData['ALBUM_NAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Song Name</span>
			<span class="textright"><?= (!empty($songData['SONG_NAME'])?$songData['SONG_NAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Song Amount</span>
			<span class="textright"><?= (!empty($songData['SONG_AMOUNT'])?$songData['SONG_AMOUNT']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Company Percentage</span>
			<span class="textright"><?= (!empty($songData['SONG_COMPANY_PERCENTAGE'])?$songData['SONG_COMPANY_PERCENTAGE']:'---'); ?></span>
		</label>
		
		<label class="informationdiv">
			<span class="textleft">Genres Type</span>
			<span class="textright"><?= (!empty($songData['GENRES_NAME'])?$songData['GENRES_NAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Mood Type</span>
			<span class="textright"><?= (!empty($songData['MOOD_NAME'])?$songData['MOOD_NAME']:'---'); ?></span>
		</label>
		
		<label class="informationdiv">
			<span class="textleft">Location</span>
			<span class="textright"><?= (!empty($songData['ALBUM_LOCATION'])?$songData['ALBUM_LOCATION']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Country</span>
			<span class="textright"><?= (!empty($songData['ALBUM_COUNTRY'])?$songData['ALBUM_COUNTRY']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">City</span>
			<span class="textright"><?= (!empty($songData['ALBUM_CITY'])?$songData['ALBUM_CITY']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Song Status</span>
			<span class="textright"><?php echo (!empty($songData['SONG_STATUS'])?'Active':'Inactive')?></span>
		</label>
   </div>
   <?php } else {echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>
<script type="text/javascript">
/** menu active script **/
$('#song_manage').addClass('open');
$('#song_manage .submenu').show();
$('#song_manage #view').addClass('submenu-color');
</script>

