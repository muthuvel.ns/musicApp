<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src = "<?php echo base_url();?>assets/js/datetimepicker_css.js"  type="text/javascript" language="javascript"></script>

<div class="form-style-2">
   <div class="form-style-2-heading">Song Management > View</div>
   <form class="innerform" action="" name="searchForm" id="searchForm">
      <label class="searchlabal" for="field1"><span>Song Name </span><input type="text" class="input-field" name="songname" value="" /></label>
      <label class="searchlabal" for="field1"><span>Song Amount </span><input type="text" class="input-field" name="song_amt" id="song_amt" value="" /></label>
 
      <label class="searchlabal" for="field4">
         <span>Album Name </span>
         <?= form_dropdown('albumId',(!empty($albumList)?$albumList:''), '', ' class="select-field" id="albumId"');?>
        <!--  <select name="field4" class="select-field" name="artistType" id="artistType">
            	<option value="">Select artist type</option>
				<option value="1">Singer</option>
				<option value="2">Composer</option>
				<option value="3">Band</option>
				<option value="4">Producer</option>
				<option value="5">Studio</option>
         </select>-->
      </label>
     
      <label class="searchlabal" for="field4">
         <span>Genres Type</span>
         <?= form_dropdown('genresType', $genresType, '', ' class="select-field" id="genresType" tabindex="4"');?>
        <!--  <select name="field4" class="select-field" name="artistType" id="artistType">
            	<option value="">Select artist type</option>
				<option value="1">Singer</option>
				<option value="2">Composer</option>
				<option value="3">Band</option>
				<option value="4">Producer</option>
				<option value="5">Studio</option>
         </select>-->
      </label>
      <label class="searchlabal" for="field4">
         <span>Mood Type</span>
         <?= form_dropdown('moodType', $moodType, '', ' class="select-field" id="moodType"');?>
        <!--  <select name="field4" class="select-field" name="artistType" id="artistType">
            	<option value="">Select artist type</option>
				<option value="1">Singer</option>
				<option value="2">Composer</option>
				<option value="3">Band</option>
				<option value="4">Producer</option>
				<option value="5">Studio</option>
         </select>-->
      </label>
       <label class="searchlabal" for="field4">
         <span>Range</span>
         <select name="SEARCH_LIMIT" id="SEARCH_LIMIT" class="select-field" onchange="showdaterange(this.value);">
            	<option value="">Select range</option>
				<option value="1">Today</option>
				<option value="2">Yesterday</option>
				<option value="3">This Week</option>
				<option value="4">Last Week</option>
				<option value="5">This Month</option>
				<option value="6">Last Month</option>
         </select>
      </label>
       <label class="searchlabal" for="field1"><span>Start Date </span>
       <input type="text"  id="START_DATE_TIME" class="input-field" name="START_DATE_TIME" value="<?PHP if(isset($_REQUEST['START_DATE_TIME'])) {echo $_REQUEST['START_DATE_TIME'];}else{ echo date("d-m-Y 00:00:00");} ?>">
       <!-- <input type="text" class="input-field" id="startdate" name="startdate" value="" />-->
       <a onclick="NewCssCal('START_DATE_TIME','ddmmyyyy','arrow',true,24,false)" href="#"><img src="<?php echo base_url(); ?>assets/images/calendar.png" /></a>
       </label>
      <label class="searchlabal" for="field2"><span>End Date </span>
      <input type="text" id="END_DATE_TIME" class="input-field" name="END_DATE_TIME" value="<?PHP if(isset($_REQUEST['END_DATE_TIME'])) {echo $_REQUEST['END_DATE_TIME'];}else{ echo date("d-m-Y 23:59:59");} ?>">
      <!-- <input type="text" class="input-field" id="enddate" name="enddate" value="" />-->
      <a onclick="javascript:NewCssCal ('END_DATE_TIME','ddmmyyyy','arrow',true,24,false)" href="#"><img src="<?php echo base_url(); ?>assets/images/calendar.png" /></a>
      </label>
      <p class="submitbtn"><span>&nbsp;</span>
      		<a href="#" class="submit" id="search">Search</a>
          <input class="submit" type="reset" value="Clear" />
       </p>
   </form>
</div>

<div class="table">
<?php // if(!empty($search)){?>
	<table id="example" class="example" cellspacing="1" border="1" width="100%" style="display: none">
	        <thead class="heading-table">
	            <tr>
	                <th>Album Name</th>
	                <th>Song Name</th>
	                <th>Amount</th>
	                <th>Company Percentage</th>
	                <th>Action</th>
	            </tr>
	        </thead>
	        <!--<tfoot>
	            <tr>
	                <th>First name</th>
	                <th>Last name</th>
	                <th>Position</th>
	                <th>Office</th>
	                <th>Start date</th>
	                <th>Salary</th>
	            </tr>
	        </tfoot>-->
          <tbody class="body-table">
          <?php /*foreach ($search as $key => $value) { ?>
            
            <tr>
                <td><?= $value['SONG_NAME'];?></td>
                <td><?= $value['ALBUM_NAME'];?></td>
                <td><?= $value['SONG_AMOUNT'];?></td>
                <td><?= $value['SONG_COMPANY_PERCENTAGE'];?></td>
                <td>
                  <a href="<?= base_url().'song/index/edit_songs/'.$value['SONG_ID'];?>" title="edit song info"> <i class="fa fa-edit"></i></a>
                  <a href="<?= base_url().'song/index/information/'.$value['SONG_ID'];?>" title="view song info"> <i class="fa fa-list-alt"></i></a>
                	
                </td>
            </tr>
            <?php 
          }*/?>
         </tbody>
	    </table>
      <?php //} else { echo "No data found"; } ?> 
</div>		

	<!--<div class="table" >
		<div class="heading">
			<div class="Username">S.No</div>
			<div class="Email">Artist Type</div>
			<div class="Partner">Name</div>
			<div class="RealMoney">Email</div>
			<div class="VIPChips">Phone</div>
			<div class="Online">Total song</div>
			<div class="Actions">Actions</div>
		</div>
		<div class="child">  <div class="Username">1</div>
			<div class="Email">singer</div>
			<div class="Partner">Partner</div>
			<div class="RealMoney">test@gmail.com</div>
			<div class="VIPChips">000000000</div>
			<div class="Online">10</div>
			<div class="Actions">view</div>
		</div>
		<div class="child">  <div class="Username">2</div>
				<div class="Email">singer</div>
			<div class="Partner">Partner</div>
			<div class="RealMoney">test@gmail.com</div>
			<div class="VIPChips">000000000</div>
			<div class="Online">10</div>
			<div class="Actions">view</div>
		</div>
		<div class="child">  <div class="Username">3</div>
				<div class="Email">singer</div>
			<div class="Partner">Partner</div>
			<div class="RealMoney">test@gmail.com</div>
			<div class="VIPChips">000000000</div>
			<div class="Online">10</div>
			<div class="Actions">view</div>
		</div>
		<div class="child">  <div class="Username">4</div>
				<div class="Email">singer</div>
			<div class="Partner">Partner</div>
			<div class="RealMoney">test@gmail.com</div>
			<div class="VIPChips">000000000</div>
			<div class="Online">10</div>
			<div class="Actions">view</div>
		</div>
	</div>-->
	
	<script>
	/** menu active script **/
	$('#song_manage').addClass('open');
	$('#song_manage .submenu').show();
	$('#song_manage #view').addClass('submenu-color');
	
	$('#search').click(function(){
		 /*   $("<table class='example' class='display' cellspacing='0' width='100%'>"
		    +"<thead class='heading-table'>"
		    +"<tr>"
		    +"<th>Artist Type</th>"
		    +"<th>Name</th>"
		    +"<th>Email</th>"
		    +"<th>Phone</th>"
		    +"<th>Total song</th>"
		    +"<th>Action</th>"
		    +"</tr>"
		    +"</thead>"
		    +"<tbody>").appendTo('#table-section');*/
		    $('.example').show();
			var str =JSON.stringify($('#searchForm').serializeObject()) ;
			$('.example').DataTable( {
				"order": [[ 0, "asc" ]],
				 "columnDefs": [ {
			          "targets": 4,
			          "orderable": false,
			    } ],
				destroy: true,
				"searching": false,
				"lengthMenu": [[25, 50, 100], [25, 50, 100]],
		        "processing": true,
		        "serverSide": true,
		        "ajax": $.fn.dataTable.pipeline( {
		            url: baseurl+'song/index/search',
		            dataType: "json",
		            data: { data: str },
		           // dataSrc: "tableData",
		            pages: 100 // number of pages to cache
		        } )
		    } );
		//	alert(str);
		});

		$.fn.serializeObject = function()
		{
		    var o = {};
		    var a = this.serializeArray();
		    $.each(a, function() {
		        if (o[this.name] !== undefined) {
		            if (!o[this.name].push) {
		                o[this.name] = [o[this.name]];
		            }
		            o[this.name].push(this.value || '');
		        } else {
		            o[this.name] = this.value || '';
		        }
		    });
		    return o;
		};
	$("#song_amt").keypress(function (e) { //digits only allowed
		if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {                 
			return false;             
		}         
	});
	
	function showdaterange(vid)
    {
      if(vid!=''){
          var sdate='';
          var edate='';
          if(vid=="1"){
              sdate='<?php echo date("d-m-Y 00:00:00");?>';
              edate='<?php echo date("d-m-Y 23:59:59");?>';
          }
          if(vid=="2"){
              <?php
              $yesterday=date('d-m-Y',strtotime("-1 days"));?>
              sdate='<?php echo $yesterday." 00:00:00";?>';
              edate='<?php echo $yesterday." 23:59:59";?>';
          }
          if(vid=="3"){
              
            
              <?php
              $sweekday=date("d-m-Y",strtotime(date("d-m-Y"))-((date("w")-1)*24*60*60));
              ?>
              //alert('<?php echo $sweekday;?>');
              sdate='<?php echo $sweekday." 00:00:00";?>';
              edate='<?php echo date("d-m-Y 23:59:59");?>';
          }
          if(vid=="4"){
             <?php
              $sweekday=date("d-m-Y",strtotime(date("d-m-Y"))-((date("w")-1)*24*60*60));
              $slastweekday=date("d-m-Y",strtotime($sweekday)-(7*24*60*60));
              $slastweekeday=date("d-m-Y",strtotime($slastweekday)+(6*24*60*60));
              ?>
              sdate='<?php echo $slastweekday." 00:00:00";?>';
              edate='<?php echo $slastweekeday." 23:59:59";?>';
          }
          if(vid=="5"){
              <?php
              $tmonth=date("m");
              $tyear=date("Y");
              $tdate="01-".$tmonth."-".$tyear;
              $lday=date('t',strtotime(date("d-m-Y")))."-".$tmonth."-".$tyear;
              //$slastweekday=date("d-m-Y",strtotime(date("d-m-Y"))-(7*24*60*60));
              ?>
              sdate='<?php echo $tdate." 00:00:00";?>';
              edate='<?php echo $lday." 23:59:59";?>';
          }
          if(vid=="6"){
              <?php
              $tmonth=date("m");
              $tyear=date("Y");
              $tdate=date("01-m-Y 00:00:00", strtotime("-1 month"));
              $lday=date("t-m-Y 23:59:59", strtotime("-1 month"));
              
              //$slastweekday=date("d-m-Y",strtotime(date("d-m-Y"))-(7*24*60*60));
              ?>
              sdate='<?php echo $tdate;?>';
              edate='<?php echo $lday;?>';
          }
          document.getElementById("START_DATE_TIME").value=sdate;
          document.getElementById("END_DATE_TIME").value=edate;
      }
    }
	</script>