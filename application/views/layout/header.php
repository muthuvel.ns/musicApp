<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html >
   <head>
      <meta charset="UTF-8">
      <title>BACK OFFICE</title>
      
  <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="<?= base_url().'assets/css/style.css'?>">
      <link rel='stylesheet prefetch' href="<?= base_url().'assets/js/dataTables/jquery.dataTables.min.css';?>">
      <script src="<?= base_url().'assets/js/jquery/jquery.min.js'?>"></script>
	<script src ="<?= base_url().'assets/js/dataTables/jquery.dataTables.min.js'?>"  type="text/javascript"></script>
    <script src="<?= base_url().'assets/js/index.js'?>"></script>
   </head>
   <body>
       <div class="header">
           
    <div class="logo">
        <p class="logotxt">BACK OFFICE</p>
           </div>
       </div>
    
<div class="sidebarmenu">
       <div class="form-style-2-heading">Main Menu</div>
     <ul id="accordion" class="accordion">
   <li class="parentli" id="dashboard">
    <a href="<?= base_url().'dashboard/index'?>"" style="text-decoration: none;"><div class="link"><i class="fa fa-user"></i>Dashboard</div></a>
  </li>
  
  <li class="parentli" id="art_manage">
    <div class="link"><i class="fa fa-code"></i>Artist Management<i class="fa fa-chevron-down"></i></div>
    <ul class="submenu">
      <li id="create"><a href="<?= base_url().'artist/index'?>">Create Artist</a></li>
      <li id="view"><a href="<?= base_url().'artist/index/view'?>">View Artist</a></li>
    </ul>
  </li>
  <li class="parentli" id="album_manage">
    <div class="link"><i class="fa fa-bar-chart"></i>Album Management<i class="fa fa-chevron-down"></i></div>
    <ul class="submenu">
      <li id="create"><a href="<?= base_url().'album/index'?>">Create Album</a></li>
      <li id="view"><a href="<?= base_url().'album/index/view'?>">View Album</a></li>
    </ul>
  </li>
  <li class="parentli" id="song_manage">
    <div class="link"><i class="fa fa-caret-square-o-right"></i>Song Management<i class="fa fa-chevron-down"></i></div>
    <ul class="submenu">
      <li id="create"><a href="<?= base_url().'song/index'?>">Create Song</a></li>
      <li id="view"><a href="<?= base_url().'song/index/view'?>">View songs</a></li>
    </ul>
  </li>
    <li class="parentli" id="user_manage">
    <a href="<?= base_url().'user/account'?>"style="text-decoration: none;"> <div class="link"><i class="fa fa-group"></i>User Management</div></a>
  </li>
  <li class="parentli" id="report">
    <div class="link"><i class="fa fa-line-chart"></i>Report<i class="fa fa-chevron-down"></i></div>
    <ul class="submenu">
      <li id="song_trans"><a href="<?= base_url().'report/songtransaction'?>">Song Transaction Report</a></li>
      <li id="subscribe"><a href="#">Subscribed Report</a></li>
	  <li id="payout"><a href="#">Payout Report</a></li>
    </ul>
  </li>
  <li class="parentli" id="settlement">
    <a href="#" style="text-decoration: none;"><div class="link"><i class="fa fa-money"></i>Settlement</div></a>
  </li>
  <li class="parentli" id="logout">
    <a href="<?= base_url().'logout'?>" style="text-decoration: none;"><div class="link"><i class="fa fa-sign-out"></i>Logout</div></a>
  </li>
</ul>
 
       </div>
 <input type="hidden" id="baseurl" value="<?= base_url()?>"/>
 <script type="text/javascript">
  var baseurl = $("#baseurl").val();
 var global_baseurl = $("#baseurl").val();

 </script>
     