<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="form-style-2">
   <div class="form-style-2-heading">Album Management > Update Album</div>
    <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i> <?php echo $flash['message']; ?>
		</span>
<?php }
		if (!empty( $albumInfo )){
?>
   <form class="innerform" action="<?= base_url().'album/index/edit/'.$albumInfo['ALBUM_ID'];?>" method="post" id="albumForm" enctype="multipart/form-data">
      <label for="field1"><span>Album Name <span class="required">*</span></span>
      	<input type="text" class="input-field" id="album_name" name="album_name"  tabindex="1" value="<?= $albumInfo['ALBUM_NAME']?>" required />
      	<input type="hidden" name="old_name" value="<?= $albumInfo['ALBUM_NAME']?>"/>
      	<label id="album_name-error" class="error error-msg" for="album_name"></label>
      </label>
      <label for="field2" style="padding-bottom: 10px;margin-bottom: 30px;"><span>Logo <span class="required">*</span></span>
		  <span style="float:left;"><img alt="LOGO" width="75" height="75" src="<?= base_url().'assets/upload_images/album/'.$albumInfo['ALBUM_LOGO']?>" id="blah"></span>
		  <input type="file"  id="logo" name="logo" tabindex="2" onchange="readURL(this);" class="upload" style="margin-top: 30px;" value="<?= $albumInfo['ALBUM_LOGO']?>"/>
		  <input type="hidden" name="old_logo" value="<?= $albumInfo['ALBUM_LOGO']?>">
		  <label id="logo-error" class="error error-msg" for="logo"></label>
	  </label>
	  
      <label for="field4">
         <span>Artist Name<span class="required">*</span></span>
          <?= form_dropdown('artistId', (!empty($artistList)?$artistList:''), $albumInfo['ALBUM_ARTISTS_ID'], ' class="select-field" id="artistId" tabindex="3"');?>
         <!-- <select name="artistId" class="select-field" tabindex="3">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select> -->
         <input type="hidden"name="old_artistId" value="<?= $albumInfo['ALBUM_ARTISTS_ID']?>"/>
         <label id="artistId-error" class="error error-msg" for="artistId"></label>
      </label>
      <label for="field4">
         <span>Genres Type<span class="required">*</span></span>
         <?= form_dropdown('genresType', $genresType, $albumInfo['ALBUM_GENRES_ID'], ' class="select-field" id="genresType" tabindex="4"');?>
         
        <!-- <select name="genresType" class="select-field" tabindex="4">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <input type="hidden"name="old_genresType" value="<?= $albumInfo['ALBUM_GENRES_ID']?>"/>
         <label id="genresType-error" class="error error-msg" for="genresType"></label>
      </label>
      <label for="field4">
         <span>Mood Type</span>
         <?= form_dropdown('moodType', $moodType, $albumInfo['ALBUM_MOOD_ID'], ' class="select-field" id="moodType" tabindex="5"');?>
         <!-- <select name="moodId" class="select-field" tabindex="5">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <input type="hidden"name="old_moodType" value="<?= $albumInfo['ALBUM_MOOD_ID']?>"/>
      </label>
      <label for="field2"><span>Location </span>
      	<input type="text" class="input-field" name="location" tabindex="6" value="<?= $albumInfo['ALBUM_LOCATION']?>"/>
      	<input type="hidden"name="old_location" value="<?= $albumInfo['ALBUM_LOCATION']?>"/>
      </label>
	  <label for="field2"><span>Country </span>
	  	<input type="text" class="input-field" name="country" tabindex="7" value="<?= $albumInfo['ALBUM_COUNTRY']?>"/>
	  	<input type="hidden"name="old_country" value="<?= $albumInfo['ALBUM_COUNTRY']?>"/>
	  </label>
	  <label for="field2"><span>City </span>
	  	<input type="text" class="input-field" name="city" tabindex="8" value="<?= $albumInfo['ALBUM_CITY'];?>"/>
	  	<input type="hidden" name="old_city" value="<?= $albumInfo['ALBUM_CITY'];?>"/>
	  </label>
	   <label for="field4">
         <span>Album Status</span>
         <select name="album_status" class="select-field" tabindex="9">
            <option value="1" <?php echo (!empty($albumInfo['ALBUM_STATUS'])?'selected="selected"':'')?>>Active</option>
            <option value="0" <?php echo (empty($albumInfo['ALBUM_STATUS'])?'selected="selected"':'')?>>Deactive</option>
         </select>
         <input type="hidden"name="old_status" value="<?= $albumInfo['ALBUM_STATUS'];?>"/>
      </label>
	  <label for="field1"><span>Podcast </span>
	  	<input type="checkbox" class="radio-field" name="podcast"  id="podcast" <?= (!empty($albumInfo['ALBUM_IS_PODCAST']) ? 'checked="checked"' :'');?> tabindex="10" />
	  	<?= form_dropdown('podcastType', $PodcastType, $albumInfo['ALUBM_PODCAST_CATEGORY'], ' class="select-field" id="podcastType" style="display:none;" tabindex="11" required');?>
	  </label>
	  
      <label><span>&nbsp;</span><input type="submit" value="Create" /></label>
   </form>
   <?php } else { echo '<h3 style="text-align: center">Information Not Found</h3>'; }?>
   
</div>
<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   
<script type="text/javascript">
/** menu active script **/
$('#album_manage').addClass('open');
$('#album_manage .submenu').show();
$('#album_manage #view').addClass('submenu-color');

function readURL(input) {
	var ext = $('#logo').val().split('.').pop().toLowerCase(); 	
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) { 		
		$('#logo').val(''); 		 
		alert('Invalid File Format.Allows Only Image File '); 	    
		return false; 	
	} 
	
	if (input.files && input.files[0]) {        
		var reader = new FileReader();         
		reader.onload = function (e) {         	
		$('#blah').attr('src', e.target.result)             
		.width(75)             
		.height(75);         
		}         
		reader.readAsDataURL(input.files[0]);     
	} 
}

$("#album_name").keypress(function (e) { //letters and digits only allowed
	if (e.which != 8 && e.which != 0 &&  e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
		return false;             
	}         
});

$(function () {
	/** trim the white space rear and frond*/
	$('input').blur(function () {                        
		$(this).val(
			$.trim($(this).val())
		);
	});

	/** podcast is checked then podcastType is showed*/
	if ($('#podcast').is(':checked')) {
		   $('#podcastType').show();
		  
	  } else {
		  $('#podcastType').hide();
	  }
});
// Wait for the DOM to be ready
$(function() {
	  $("#albumForm").validate({
	    rules: {
	      	album_name: "required",
	      	genresType: "required",
	      	artistId: "required",
// 	      	logo: "required",
	    	podcastType: "required"
	    },
	    messages: {
	    	album_name: "Please enter your album name",
	    	genresType: "Please select genres type",
	    	artistId: "Please select artist name",
// 	     	logo: "Please add a image",
	     	podcastType: "Please select podcast type"
	    },
	    submitHandler: function(form) {
	      form.submit();
	    }
	  });
	});

$('#podcast').change(function() {
	  if ($(this).is(':checked')) {
	   $('#podcastType').show();
	  
	  } else {
		  $('#podcastType').hide();
		  $('#podcastType').val('');
	  }
});

$('#album_name').change(function() {
	$.trim($(this).val());
	return true;
});

</script>