<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading">Album Management > View Album
	   <?php if (!empty( $albumInfo['ALBUM_ID'])){?>
	   <a style="float: right; text-decoration: none;" href="<?= base_url().'artist/index/edit_artist/'.$albumInfo['ALBUM_ID'];?>">Edit</a> 
	   <?php } ?>
   </div>
<?php 
		if (!empty( $albumInfo )){//echo '<pre>';print_r($albumInfo);exit;
?>
		<label for="field2" style="margin-left: 15px;">
		  <span><img alt="LOGO" width="75" height="75" src="<?= base_url().'assets/upload_images/album/'.$albumInfo['ALBUM_LOGO'];?>" id="blah"></span>
	  	</label>
	  	<div style="margin: 20px 0px 60px 120px;">
	  	<label class="informationdiv">
			<span class="textleft">Album Name</span>
			<span class="textright"><?= (!empty($albumInfo['ALBUM_NAME'])?$albumInfo['ALBUM_NAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Artist Name</span>
			<span class="textright"><?= (!empty($albumInfo['ARTISTS_USERNAME'])?$albumInfo['ARTISTS_USERNAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Genres Type</span>
			<span class="textright"><?= (!empty($albumInfo['GENRES_NAME'])?$albumInfo['GENRES_NAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Mood Type</span>
			<span class="textright"><?= (!empty($albumInfo['MOOD_NAME'])?$albumInfo['MOOD_NAME']:'---'); ?></span>
		</label>
		
		<label class="informationdiv">
			<span class="textleft">Location</span>
			<span class="textright"><?= (!empty($albumInfo['ALBUM_LOCATION'])?$albumInfo['ALBUM_LOCATION']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Country</span>
			<span class="textright"><?= (!empty($albumInfo['ALBUM_COUNTRY'])?$albumInfo['ALBUM_COUNTRY']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">City</span>
			<span class="textright"><?= (!empty($albumInfo['ALBUM_CITY'])?$albumInfo['ALBUM_CITY']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Album Status</span>
			<span class="textright"><?= (!empty($albumInfo['ALBUM_STATUS'])?'Active':'Inactive'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Podcast Category</span>
			<span class="textright"><?= (!empty($albumInfo['PODCAST_NAME'])?$albumInfo['PODCAST_NAME']:'---'); ?></span>
		</label>
   </div>
   <?php } else {echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>

<script type="text/javascript">

/** menu active script **/
$('#album_manage').addClass('open');
$('#album_manage .submenu').show();
$('#album_manage #view').addClass('submenu-color');
</script>
