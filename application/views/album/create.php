<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading">Album Management > Create Album</div>
    <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i> <?php echo $flash['message']; ?>
		</span>
<?php }?>
   <form class="innerform" action="<?= base_url().'album/index/add';?>" method="post" id="albumForm" enctype="multipart/form-data">
      <label for="album_name"><span>Album Name <span class="required">*</span></span>
      	<input type="text" class="input-field" id="album_name" name="album_name"  tabindex="1" required />
      		<label id="album_name-error" class="error error-msg" for="album_name"></label>
      	</label>
      <label for="logo"><span>Logo <span class="required">*</span></span>
      	<input type="file" class="input-field" name="logo" id="logo" onchange="readURL(this);" tabindex="2" required/>
      	<label id="logo-error" class="error error-msg" for="logo"></label>
      </label>
      <label for="field4">
         <span>Artist Name <span class="required">*</span></span>
          <?= form_dropdown('artistId', (!empty($artistList)?$artistList:''), '', ' class="select-field" id="artistId" tabindex="3" required');?>
         <!-- <select name="artistId" class="select-field" tabindex="3">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select> -->
         <label id="artistId-error" class="error error-msg" for="artistId"></label>
      </label>
      <label for="field4">
         <span>Genres Type <span class="required">*</span></span>
         <?= form_dropdown('genresType', $genresType, '', ' class="select-field" id="genresType" tabindex="4" required');?>
         
        <!-- <select name="genresType" class="select-field" tabindex="4">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
         <label id="genresType-error" class="error error-msg" for="genresType"></label>
      </label>
      <label for="field4">
         <span>Mood Type</span>
         <?= form_dropdown('moodType', $moodType, '', ' class="select-field" id="moodType" tabindex="5"');?>
         <!-- <select name="moodId" class="select-field" tabindex="5">
            <option value="General Question">General</option>
            <option value="Advertise">Advertisement</option>
            <option value="Partnership">Partnership</option>
         </select>-->
      </label>
      <label for="field2"><span>Location </span><input type="text" class="input-field" name="location" tabindex="6"/></label>
	  <label for="field2"><span>Country </span><input type="text" class="input-field" name="country" tabindex="7"/></label>
	  <label for="field2"><span>City </span><input type="text" class="input-field" name="city" tabindex="8"/></label>
	   <label for="field4">
         <span>Album Status</span>
         <select name="album_status" class="select-field" tabindex="9">
            <option value="1" selected="selected">Active</option>
            <option value="0">Deactive</option>
         </select>
      </label>
	  <label for="field1"><span>Podcast </span>
	  	<input type="checkbox" class="radio-field" name="podcast"  id="podcast" tabindex="10" />
	  	<?= form_dropdown('podcastType', $PodcastType, '', ' class="select-field" id="podcastType" style="display:none;" tabindex="11" required');?>
	  </label>
	  
      <label class="submit-btn"><span>&nbsp;</span><input type="submit" value="Create" /></label>
   </form>
</div>
<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   
<script type="text/javascript">
/** menu active script **/
$('#album_manage').addClass('open');
$('#album_manage .submenu').show();
$('#album_manage #create').addClass('submenu-color');

function readURL(input) {
	var ext = $('#logo').val().split('.').pop().toLowerCase(); 	
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) { 		
		$('#logo').val(''); 		 
		alert('Invalid File Format.Allows Only Image File '); 	    
		return false; 	
	} 
}

$("#album_name").keypress(function (e) { //letters and digits only allowed
	if (e.which != 8 && e.which != 0 &&  e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
		return false;             
	}         
});
$(function () {
	$('input').blur(function () {                        
		$(this).val(
			$.trim($(this).val())
		);
	});
});
// Wait for the DOM to be ready
$(function() {
	  $("#albumForm").validate({
	    rules: {
	      	album_name: "required",
	      	logo: "required",
	      	artistId: "required",
	      	genresType: "required",
	    	podcastType: "required"
	    },
	    messages: {
	    	album_name: "Please enter your album name",
	     	logo: "Please add a image",
	     	artistId: "Please select artist name",
	     	genresType: "Please select genres type",
	     	podcastType: "Please select podcast type"
	    },
	    submitHandler: function(form) {
	      form.submit();
	    }
	  });
	});

$('#podcast').change(function() {
	  if ($(this).is(':checked')) {
	   $('#podcastType').show();
	  
	  } else {
		  $('#podcastType').hide();
	  }
});

$('#album_name').change(function() {
	$.trim($(this).val());
	return true;
});

</script>