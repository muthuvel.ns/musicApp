<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading">Artist Management > Update Artist</div>
   <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i> <?php echo $flash['message']; ?>
		</span>
<?php }
		if (!empty( $artistData )){
?>
   <form class="innerform" name="regForm" id="regForm" action="<?= base_url().'artist/index/edit/'.$artistData['ARTISTS_ID']?>" method="post" enctype="multipart/form-data">
		<label>
			<span>Artist Type<span class="required">*</span></span>
			<?= form_dropdown('artistType', $artistType, $artistData['ARTISTS_TYPE_ID'], ' class="select-field" id="artistType" tabindex="1" required');?>
			<!-- <select class="select-field" name="artistType" id="artistType" tabindex="1" required>
				<option value="">Select artist type</option>
				<option value="1">Singer</option>
				<option value="2">Composer</option>
				<option value="3">Band</option>
				<option value="4">Producer</option>
				<option value="5">Studio</option>
			</select>-->
			<input type="hidden" name="old_artistType" value="<?= $artistData['ARTISTS_TYPE_ID'];?>">
			<label id="artistType-error" class="error error-msg" for="artistType"></label>
		</label>
      <label for="field1"><span>User Name <span class="required">*</span></span><input type="text" class="input-field" tabindex="2" maxlength="30" onchange="checkUsername(this.value,'<?= $artistData['ARTISTS_ID'] ?>')" id="username" name="username" value="<?= $artistData['ARTISTS_USERNAME']?>" required/>
	      <input type="hidden" name="old_username" value="<?= $artistData['ARTISTS_USERNAME'];?>">
	      <label id="username-error" class="error error-msg" for="username"></label>
      </label>
	  <label for="field1"><span>Password </span>
	  	<input type="password" name="password" class="input-field" tabindex="3" />
	  	<input type="hidden" name="old_password" value="<?= $artistData['ARTISTS_PASSWORD'];?>">
	  	<label id="password-error" class="error error-msg" for="password"></label>
	  </label>
	  <label for="field1"><span>First Name </span>
	  	<input type="text" class="input-field" name="fname" tabindex="4" value="<?= $artistData['ARTISTS_FIRSTNAME']?>" />
	  	<input type="hidden" name="old_fname" value="<?= $artistData['ARTISTS_FIRSTNAME'];?>">
	  </label>
	  <label for="field1"><span>Last Name </span>
	  	<input type="text" class="input-field" name="lname" tabindex="5" value="<?= $artistData['ARTISTS_LASTNAME']?>" />
	  	<input type="hidden" name="old_lname" value="<?= $artistData['ARTISTS_LASTNAME'];?>">
	  </label>
      <label for="field2"><span>Email <span class="required">*</span></span>
      	<input type="text" class="input-field" tabindex="6" maxlength="30" value="<?= $artistData['ARTISTS_EMAIL']?>" onchange="checkemail(this.value,'<?= $artistData['ARTISTS_ID'] ?>')" id="email" name="email" required/>
      	<input type="hidden" name="old_email" value="<?= $artistData['ARTISTS_EMAIL'];?>">
      	<label id="email-error" class="error error-msg" for="email"></label>
      </label>
	  <label for="field2"><span>Mobile </span>
	  	<input type="text" class="input-field" name="mobile" id="mobile" tabindex="7" maxlength="12" value="<?= $artistData['ARTISTS_PHONE']?>" />
	  	<input type="hidden" name="old_mobile" value="<?= $artistData['ARTISTS_PHONE'];?>">
	  	</label>
	  <label for="field2" style="padding-bottom: 10px;margin-bottom: 30px;"><span>Image </span>
		  <span style="float:left;"><img alt="" width="75" height="75" src="<?= base_url().'assets/upload_images/artist/'.$artistData['ARTISTS_IMAGE'];?>" id="blah"></span>
		  <input type="file"  id="image" name="image" tabindex="8" onchange="readURL(this);" class="upload" style="margin-top: 30px;"/>
		  <input type="hidden" name="old_image" value="<?= $artistData['ARTISTS_IMAGE'];?>">
	  </label>
	  <label for="field2"><span>Location </span>
	  	<input type="text" class="input-field" name="location" tabindex="9" value="<?= $artistData['ARTISTS_LOCATION']?>"/>
	  	<input type="hidden" name="old_location" value="<?= $artistData['ARTISTS_LOCATION'];?>">
	  </label>
	  <label for="field2"><span>Country </span>
	  	<input type="text" class="input-field" name="country" tabindex="10" value="<?= $artistData['ARTISTS_COUNTRY']?>"/>
	  	<input type="hidden" name="old_country" value="<?= $artistData['ARTISTS_COUNTRY'];?>">
	  </label>
	  <label for="field2"><span>City </span>
	  	<input type="text" class="input-field" name="city" tabindex="11" value="<?= $artistData['ARTISTS_CITY']?>"/>
	  	<input type="hidden" name="old_city" value="<?= $artistData['ARTISTS_CITY'];?>">
	  </label>
	  <label for="field4">
         <span>Album Status</span>
         <select name="artist_status" class="select-field" tabindex="12">
            <option value="1" <?php echo (!empty($artistData['ARTISTS_STATUS'])?'selected="selected"':'')?>>Active</option>
            <option value="0" <?php echo (empty($artistData['ARTISTS_STATUS'])?'selected="selected"':'')?>>Inactive</option>
         </select>
         <input type="hidden" name="old_status" value="<?= $artistData['ARTISTS_STATUS'];?>">
      </label>
      
      <label><span>&nbsp;</span><input type="submit" value="Update" /></label>
   </form>
   <?php } else { echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>

<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   

<script type="text/javascript">
/** menu active script **/
$('#art_manage').addClass('open');
$('#art_manage .submenu').show();
$('#art_manage #view').addClass('submenu-color');

function readURL(input) {
	var ext = $('#image').val().split('.').pop().toLowerCase(); 	
		if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) { 		
		$('#image').val(''); 		 
		$('#blah').attr('src', ''); 
		alert('Invalid File Format.Allows Only Image File '); 	    
		return false; 	
	} 	
	if (input.files && input.files[0]) {        
		var reader = new FileReader();         
		reader.onload = function (e) {         	
		$('#blah').attr('src', e.target.result)             
		.width(75)             
		.height(75);         
		}         
		reader.readAsDataURL(input.files[0]);     
	} 
}


/*$(document).ready(function(){
	var ruleSet1 = {
        required: true,
        alphanumeric: true
    };
	var ruleSet2 = {
		required: true,
		email: true
	};
	var ruleSet3 = {
	        required: true
	 };
	$('#regForm').validate({
		rules: {
		artistType:ruleSet3,
		username: ruleSet1,
		email: ruleSet2,
		password: ruleSet3
		
  	},
	messages: {
		artistType: "Selcet any one type",
		username: "Enter username( alpha numeric) ",
		email: "Enter valid email id",
		password: "Enter password"
    }
 });
}); // end document.ready
*/

/** remove space input filed rear and end**/
$(function () {
	$('input').blur(function () {                        
		$(this).val(
			$.trim($(this).val())
		);
	});
});

$("#mobile").keypress(function (e) { //digits only allowed
	if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});

$("#username").keypress(function (e) { //letters and digits only allowed
	if (e.which != 8 && e.which != 0 && e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
		return false;             
	}         
});

// Wait for the DOM to be ready
$(function() {
	  // Initialize form validation on the registration form.
	  // It has the name attribute "registration"
	  $("#regForm").validate({
	    // Specify validation rules
	    rules: {
	      // The key name on the left side is the name attribute
	      // of an input field. Validation rules are defined
	      // on the right side
	      artistType: "required",
	      username: {
		        required: true
		      },
	      email: {
	        required: true,
	        // Specify that email should be validated
	        // by the built-in "email" rule
	        email: true
	      },
	      password: {
	        //required: true,
	        minlength: 5
	      }
	    },
	    // Specify validation error messages
	    messages: {
	    	artistType: "Please select artist type",
	    	username: "Please enter your username",
	      password: {
	        required: "Please provide a password",
	        minlength: "Your password must be at least 5 characters long"
	      },
	      email: "Please enter a valid email address"
	    },
	    // Make sure the form is submitted to the destination defined
	    // in the "action" attribute of the form when valid
	    submitHandler: function(form) {
	      form.submit();
	    }
	  });
	});

function checkemail(email, userId ){
	  var emailId = $.trim(email);
	  if(emailId==''||emailId==0 || userId==''||userId==0){
		  return false;
	  }
	  
	   $.ajax({
			  type:"post",
			  url:baseurl+"artist/index/emailavailability?id="+userId,
			  data:"email="+emailId,
			  dataType:"html",
			  success:function(jdata){
				  if( jdata == 1 ){ 		
				 	$('#email').val('');
				 	$('#email-error').html('email already available');
				 	$('#email-error').show();
				 	 return false; 	
				  } else if( jdata == 2 ){
					  $('#email-error').html('');
				  }
		  	}
	   });
}

function checkUsername( uname, userId ){
	  var username = $.trim(uname);
	  if(username==''||username==0 || userId==''||userId==0){
		  return false;
	  }

	   $.ajax({
			  type:"post",
			  url:baseurl+"artist/index/useravailability?id="+userId,
			  data:"uname="+username,
			  dataType:"html",
			  success:function(jdata){
				  if( jdata == 1 ){ 		
				 	$('#username').val('');
				 	$('#username-error').html('Username already available');
				 	$('#username-error').show();
				 	 return false; 	
				  } else if( jdata == 2 ){
					  $('#username-error').html('');
				  }
		 	 }
	   });
}
</script>
