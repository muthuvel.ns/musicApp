<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading">Artist Management > View Artist 
	   <?php if (!empty( $artistData['ARTISTS_ID'])){?>
	   <a style="float: right; text-decoration: none;" href="<?= base_url().'artist/index/edit_artist/'.$artistData['ARTISTS_ID'];?>">Edit</a>
	   <?php } ?>
   </div>
   
<?php 
		if (!empty( $artistData )){//echo '<pre>';print_r($artistData);exit;
?>
		<label for="field2" style="margin-left: 15px;">
		  <span><img alt="IMAGE" width="75" height="75" src="<?= base_url().'assets/upload_images/artist/'.$artistData['ARTISTS_IMAGE'];?>" id="blah"></span>
	  	</label>
	  	<div style="margin: 20px 0px 60px 120px;">
			<label class="informationdiv">
				<span class="textleft">Artist Type</span>
				<span class="textright"><?= (!empty($artistData['ARTISTS_TYPE_NAME'])?$artistData['ARTISTS_TYPE_NAME']:'---'); ?></span>
			</label>
				
	      <label class="informationdiv"><span class="textleft">User Name </span>
			<span class="textright"><?= (!empty($artistData['ARTISTS_USERNAME'])?$artistData['ARTISTS_USERNAME']:'---');?></span>
	      </label>
		  <label class="informationdiv"><span class="textleft">First Name </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_FIRSTNAME'])?$artistData['ARTISTS_FIRSTNAME']:'---');?></span>
		  </label>
		  <label class="informationdiv"><span class="textleft">Last Name </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_LASTNAME'])?$artistData['ARTISTS_LASTNAME']:'---');?></span>
		  </label>
	      <label class="informationdiv"><span class="textleft">Email <span class="required">*</span></span>
	      	<span class="textright"><?= (!empty($artistData['ARTISTS_EMAIL'])?$artistData['ARTISTS_EMAIL']:'---');?></span>
	      </label>
		  <label class="informationdiv"><span class="textleft">Mobile </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_PHONE'])?$artistData['ARTISTS_PHONE']:'---');?></span>
		  	</label>
		  <label class="informationdiv"><span class="textleft">Location </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_LOCATION'])?$artistData['ARTISTS_LOCATION']:'---');?></span>
		  </label>
		  <label class="informationdiv"><span class="textleft">Country </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_COUNTRY'])?$artistData['ARTISTS_COUNTRY']:'---');?></span>
		  </label>
		  <label class="informationdiv"><span class="textleft">City </span>
		  	<span class="textright"><?= (!empty($artistData['ARTISTS_CITY'])?$artistData['ARTISTS_CITY']:'---');?></span>
		  </label>
		  <label class="informationdiv">
	         <span class="textleft">Album Status</span>
	         <span class="textright"><?php echo (!empty($artistData['ARTISTS_STATUS'])?'Active':'Inactive')?></span>
	      </label>
   </div>
   <?php } else {echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>

<script type="text/javascript">

/** menu active script **/
$('#art_manage').addClass('open');
$('#art_manage .submenu').show();
$('#art_manage #view').addClass('submenu-color');
</script>

