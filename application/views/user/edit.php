<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script src = "<?php echo base_url();?>assets/js/datetimepicker_css.js"  type="text/javascript" language="javascript"></script>
<div class="form-style-2">
   <div class="form-style-2-heading">User Management > Update User</div>
 <?php
		if(!empty($flash['message'])){
	?>
		<script>
			/*setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);*/
		</script>
		<span id="error_msg" class="<?= $flash['class'] ?>" > 
			<i class="fa fa-info-circle" ></i> <?php echo $flash['message']; ?>
		</span>
<?php }
		if (!empty( $userData )){
?>
   <form class="innerform" name="regForm" id="regForm" action="<?= base_url().'user/account/edit/'.$userData['USER_ID']?>" method="post" enctype="multipart/form-data">
      <label for="field1"><span>User Name <span class="required">*</span></span><input type="text" class="input-field" tabindex="1" maxlength="30" onchange="checkUsername(this.value,'<?= $userData['USER_ID'] ?>')" id="username" name="username" value="<?= $userData['USER_USERNAME']?>" required/>
	      <input type="hidden" name="old_username" value="<?= $userData['USER_USERNAME'];?>">
	      <label id="username-error" class="error error-msg" for="username"></label>
      </label>
	  <label for="field1"><span>Password </span>
	  	<input type="password" name="password" class="input-field" tabindex="2" />
	  	<input type="hidden" name="old_password" value="<?= $userData['USER_PASSWORD'];?>">
	  	<label id="password-error" class="error error-msg" for="password"></label>
	  </label>
	   <label for="field2"><span>Email <span class="required">*</span></span>
      	<input type="text" class="input-field" tabindex="3" maxlength="30" value="<?= $userData['USER_EMAIL']?>" onchange="checkemail(this.value,'<?= $userData['USER_ID'] ?>')" id="email" name="email" required/>
      	<input type="hidden" name="old_email" value="<?= $userData['USER_EMAIL'];?>">
      	<label id="email-error" class="error error-msg" for="email"></label>
      </label>
	  <label for="field1"><span>DOB</span>
       <input type="text"  id="dob" class="input-field" name="dob" value="<?= date( DOB_DATE_FORMAT, strtotime( $userData['USER_DOB']) );?>" tabindex="4">
       <!-- <input type="text" class="input-field" id="startdate" name="startdate" value="" />-->
       <a onclick="NewCssCal('dob','ddmmyyyy','arrow','','',false)" href="#"><img src="<?php echo base_url(); ?>assets/images/calendar.png" /></a>
	  	<input type="hidden" name="old_dob" value="<?= date( DOB_DATE_FORMAT, strtotime( $userData['USER_DOB']) );?>">
	  </label>
	  <label for="field1"><span>Gender</span>
	  	 <select name="gender" id="gender" class="select-field" tabindex="5">
            	<option value="">Select Gender</option>
				<option value="1">Male</option>
				<option value="2">Female</option>
				<option value="0">others</option>
         </select>
         <input type="hidden" name="old_gender" value="<?= $userData['USER_GENDER'];?>">
	  </label>
     
	  <label for="field2"><span>Mobile </span>
	  	<input type="text" class="input-field" name="mobile" id="mobile" tabindex="6" maxlength="12" value="<?= $userData['USER_PHONE']?>" />
	  	<input type="hidden" name="old_mobile" value="<?= $userData['USER_PHONE'];?>">
	  	</label>
	  <label for="field4">
         <span>User Status</span>
         <select name="user_status" class="select-field" tabindex="7">
            <option value="1" <?php echo (!empty($userData['USER_STATUS'])?'selected="selected"':'')?>>Active</option>
            <option value="0" <?php echo (empty($userData['USER_STATUS'])?'selected="selected"':'')?>>Inactive</option>
         </select>
         <input type="hidden" name="old_status" value="<?= $userData['USER_STATUS'];?>">
      </label>
      
      <label><span>&nbsp;</span><input type="submit" value="Update" /></label>
   </form>
   <?php } else { echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>

<script src="<?= base_url().'assets/js/validate/jquery.validate.min.js'?>""></script>   

<script type="text/javascript">
/** menu active script **/
$('#user_manage').addClass('open');
$('#user_manage .submenu').show();
$('#user_manage #view').addClass('submenu-color');


/** remove space input filed rear and end**/
$(function () {
	$('input').blur(function () {                        
		$(this).val(
			$.trim($(this).val())
		);
	});
	$('#gender').val('<?= (!empty($userData['USER_GENDER'])?$userData['USER_GENDER']:0); ?>').change();
});

$("#mobile").keypress(function (e) { //digits only allowed
	if (e.which != 8 && e.which != 0 && ((e.which < 48) || (e.which > 57))) {                 
		return false;             
	}         
});

$("#username").keypress(function (e) { //letters and digits only allowed
	if (e.which != 8 && e.which != 0 && e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
		return false;             
	}         
});

// Wait for the DOM to be ready
$(function() {
	  // Initialize form validation on the registration form.
	  // It has the name attribute "registration"
	  $("#regForm").validate({
	    // Specify validation rules
	    rules: {
	      // The key name on the left side is the name attribute
	      // of an input field. Validation rules are defined
	      // on the right side
	      username: {
		        required: true
		      },
	      email: {
	        required: true,
	        // Specify that email should be validated
	        // by the built-in "email" rule
	        email: true
	      },
	      password: {
	        //required: true,
	        minlength: 5
	      }
	    },
	    // Specify validation error messages
	    messages: {
	    	username: "Please enter your username",
	      password: {
	        required: "Please provide a password",
	        minlength: "Your password must be at least 5 characters long"
	      },
	      email: "Please enter a valid email address"
	    },
	    // Make sure the form is submitted to the destination defined
	    // in the "action" attribute of the form when valid
	    submitHandler: function(form) {
	      form.submit();
	    }
	  });
	});

function checkemail(email, userId ){
	  var emailId = $.trim(email);
	  if(emailId==''||emailId==0 || userId==''||userId==0){
		  return false;
	  }
	  
	   $.ajax({
			  type:"post",
			  url:baseurl+"user/account/emailavailability?id="+userId,
			  data:"email="+emailId,
			  dataType:"html",
			  success:function(jdata){
				  if( jdata == 1 ){ 		
				 	$('#email').val('');
				 	$('#email-error').html('email already available');
				 	$('#email-error').show();
				 	 return false; 	
				  } else if( jdata == 2 ){
					  $('#email-error').html('');
				  }
		  	}
	   });
}

function checkUsername(uname){
	  var username = $.trim(uname);
	  if(username==''||username==0 || userId==''||userId==0){
		  return false;
	  }

	   $.ajax({
			  type:"post",
			  url:baseurl+"user/account/useravailability?id="+userId,
			  data:"uname="+username,
			  dataType:"html",
			  success:function(jdata){
				  if( jdata == 1 ){ 		
				 	$('#username').val('');
				 	$('#username-error').html('Username already available');
				 	$('#username-error').show();
				 	 return false; 	
				  } else if( jdata == 2 ){
					  $('#username-error').html('');
				  }
		 	 }
	   });
}
</script>
