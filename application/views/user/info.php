<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="form-style-2">
   <div class="form-style-2-heading"><span>User Management</span> >> <span>User Information</span>
	   <?php  if (!empty( $userData['USER_ID'])){?>
	   <a style="float: right; text-decoration: none;" href="<?= base_url().'user/account/edit_user/'.$userData['USER_ID'];?>">Edit</a>
	   <?php } ?>
   </div>
   
<?php 
		if (!empty( $userData )){//echo '<pre>';print_r($userData);exit;
?>
	  	<div style="margin: 20px 0px 60px 60px;">
	  	<label class="informationdiv">
			<span class="textleft">User Name</span>
			<span class="textright"><?= (!empty($userData['USER_USERNAME'])?$userData['USER_USERNAME']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Email</span>
			<span class="textright"><?= (!empty($userData['USER_EMAIL'])?$userData['USER_EMAIL']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">DOB</span>
			<span class="textright"><?= (!empty($userData['USER_DOB'])?$userData['USER_DOB']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Gender</span>
			<span class="textright"><?= (isset($userData['USER_GENDER'])?constant("GENDER_".$userData['USER_GENDER']):'---');?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">Mobile</span>
			<span class="textright"><?= (!empty($userData['USER_PHONE'])?$artistData['USER_PHONE']:'---'); ?></span>
		</label>
		<label class="informationdiv">
			<span class="textleft">User Status</span>
			<span class="textright"> <?php echo (isset($userData['USER_STATUS'])?constant("STATUS_".$userData['USER_STATUS']):'---')?></span>
		</label>
		
   </div>
   <?php } else {echo '<h3 style="text-align: center">Information Not Found</h3>';}?>
</div>

<script type="text/javascript">

/** menu active script **/
$('#user_manage').addClass('open');
</script>

