<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<style>
.error_msg, .form-error{
	color:red;
}
</style>
<div class="login-page">
 <div class="form">
	<form class="login-form" action="" method="post">
	   <p class="user_login">User Login</p>
<?php
		if(!empty($message)){
	?>
		<script>
		//$(document).ready(function () {
			setTimeout(function() {
				$("#error_msg").hide();
			}, 2000);
		//});
	</script>
		<span id="error_msg" class="error_msg" > <?php echo $message; ?></span>
<?php }?>
		<div class="form-group">
	   		<input  type="text" placeholder="username" name="username" id="username" data-validation="alphanumeric" data-validation-error-msg="Please enter valid usename" autofocus />
	   		<span class="required-server"><?php echo form_error('username','<p style="color:#F83A18">','</p>'); ?></span> 
	   </div>
	   <div class="form-group">
	   		<input type="password" placeholder="password"/ name="password" data-validation="required" data-validation-error-msg="Please enter Password"/>
	   		<span class="required-server"><?php echo form_error('password','<p style="color:#F83A18">','</p>'); ?></span>
	  </div>
	  <div class="form-group">
	    <label for="captcha" id="captcha-img"><?php echo (!empty($captcha['image'])?$captcha['image']:''); ?>
	    </label>
	     <a href='#' class ='refresh' onclick="refreshCaptcha()"><img id ='ref_symbol' src ="<?= base_url().'assets/images/refresh.png';?>"></a>
	    <br>
	    <input type="text" autocomplete="off" name="userCaptcha" placeholder="Enter above text" value="<?php if(!empty($userCaptcha)){ echo $userCaptcha;} ?>" />
	    <span class="required-server"><?php echo form_error('userCaptcha','<p style="color:#F83A18">','</p>'); ?></span> 
	   </div>
	   <button>login</button>
	   <!--<input type="submit" name="login" value="Login">-->
	</form>
 </div>
</div>
<script src="<?= base_url().'assets/js/validate/jquery.form-validator.min.js'?> ?>"></script>
<script>
  $.validate({
   
   
  });
/* $("#username").keypress(function (e) { //letters and digits only allowed
	if (e.which != 8 && e.which != 0 &&  e.which != 32 && ((e.which < 48) || (e.which > 57)) && ((e.which < 97) || (e.which > 122))) {                 
		return false;             
	}         
});*/

//Ajax post for refresh captcha image.
/* $(document).ready(function() {
    $("a.refresh").click(function() {
        jQuery.ajax({
            type: "POST",
            url: baseurl+ "/login/captcha_refresh",
            success: function(res) {
                if (res)
                {
                      jQuery("#captcha-img").html(res);
                }
            }
        });
    });
});*/
function refreshCaptcha(){
	   $.ajax({
			  type:"post",
			  url:baseurl+"/login/captcha_refresh",
			  dataType:"json",
			  success:function(response){//console.log(response);
				  if (response.image) {
	                   $("#captcha-img").html(response.image);
	              }
		  	}
	   });
}
</script>

