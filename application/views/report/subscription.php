<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="form-style-2">
   <div class="form-style-2-heading">Report > Subscribed </div>
   <form class="innerform" action="" method="post">
      <label class="searchlabal" for="field1"><span>User Name<span class="required">*</span></span><input type="text" class="input-field" name="field1" value="" /></label>
	  <label class="searchlabal" for="field2"><span>Start Date<span class="required">*</span></span><input type="text" class="input-field" name="field2" value="" /></label>
	  <label class="searchlabal" for="field2"><span>End Date <span class="required">*</span></span><input type="text" class="input-field" name="field2" value="" /></label>
     
      <label class="searchlabal" for="field4">
         <span>Date Range</span>
         <select name="field4" class="select-field">
            <option value="">Select</option>
				<option value="1">Today</option>
				<option value="2">Yesterday</option>
				<option value="3">This Week</option>
				<option value="4">Last Week</option>
				<option value="5">This Month</option>
				<option value="6">Last Month</option>
        </select>
      </label>
      
      <p class="submitbtn"><span>&nbsp;</span><input class="submit" type="submit" value="Submit" />
          <input class="submit" type="submit" value="Clear" />
       </p>
   </form>
</div>

	<div class="table" >
		<div class="heading"> 
			<div class="coumn1">User Name</div>
			<div class="coumn2">Amount</div>
			<div class="coumn3">Date of subscription</div>
			<div class="coumn4">Expiry Date</div>
		</div>
		<div class="child">  
			<div class="coumn1">new songs</div>
			<div class="coumn2">1000</div>
			<div class="coumn3">00-00-0000</div>
			<div class="coumn4">00-00-0000</div>
		</div>
		<div class="child">  
			<div class="coumn1">new songs</div>
			<div class="coumn2">1000</div>
			<div class="coumn3">00-00-0000</div>
			<div class="coumn4">00-00-0000</div>
		</div>
		<div class="child">  
			<div class="coumn1">new songs</div>
			<div class="coumn2">1000</div>
			<div class="coumn3">00-00-0000</div>
			<div class="coumn4">00-00-0000</div>
		</div>
		<div class="child">  
			<div class="coumn1">new songs</div>
			<div class="coumn2">1000</div>
			<div class="coumn3">00-00-0000</div>
			<div class="coumn4">00-00-0000</div>
		</div>
	</div>
	
	<script>
/** menu active script **/
$('#report').addClass('open');
$('#report .submenu').show();
$('#report #subscribe').addClass('submenu-color');

</script>