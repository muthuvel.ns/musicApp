<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Index extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model("common/Common_model");
		$this->load->model("artist/Artist_model");
		$this->load->model("album/Album_model");
		$this->load->model("song/Song_model");
		$this->load->model("song/Manager_model");
		$this->load->model("event_handler/Event_model");
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	/**
	 * Song creation page
	 */
	function index() {
		/** variable declaration*/
		$this->data	= '';
		
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
		
		/** get album name list */
		$album['']='Select name';
		$albumDetails	=	$this->Album_model->albumDetail( );
		if(!empty($albumDetails)){
			foreach ($albumDetails as $id=>$val){
				$album[$val['ALBUM_ID']]=$val['ALBUM_NAME'];
			}
		}
		$this->data['albumList'] = $album;
		/** get album name end */
		
		/** get gentres type list */
		$gentres['']='Select type';
		$gentresType	=	$this->Common_model->getGenresType( );
		if(!empty($gentresType)){
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
		}
		$this->data['genresType'] = $gentres;
		/** get gentres type list end*/
		
		/** get mood type list */
		$mood['']='Select type';
		$moodType	=	$this->Common_model->getMoodType( );
		if(!empty($moodType)){
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
		}
		$this->data['moodType'] = $mood;
		/** get mood type list  end */
		
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist=array();
		if(!empty($type)){
			foreach ($type as $id=>$val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
		}
		$this->data['artistType'] = $atrist;
		/** get artist type list End */
		
		/** get artist name list */
		$atrist1['']='Select name';
		$artistDetails	=	$this->Artist_model->getArtistDetail( );
		if(!empty($artistDetails)){
			foreach ($artistDetails as $id=>$val){
				$atrist1[$val['ARTISTS_ID']]=$val['ARTISTS_USERNAME'];
			}
		}
		$this->data['artistList'] = $atrist1;
		/** get gentres type list */
		
		/** get Podcast type list */
		$Podcast	=	$this->Common_model->getPodcastType( );
		$PodcastType['']='Select type';
		if(!empty($Podcast)){
			foreach ($Podcast as $id=>$val){
				$PodcastType[$val['PODCAST_ID']]=$val['PODCAST_NAME'];
			}
		}
		$this->data['PodcastType'] = $PodcastType;
		/** get Podcast type list end*/

		$this->load->view('layout/header');
		$this->load->view('song/create', $this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Song search and view page
	 */
	function view() {

		/** get artist name list */
		$albumDetails	=	$this->Album_model->albumDetail( );
		$album['']='Select name';
		if(!empty($albumDetails)){
			foreach ($albumDetails as $id=>$val){
				$album[$val['ALBUM_ID']]=$val['ALBUM_NAME'];
			}
		}
		/** get gentres type list */
		$this->data['albumList'] = $album;
		
		/** get gentres type list */
		$gentresType	=	$this->Common_model->getGenresType( );
		$gentres['']='Select type';
		if(!empty($gentresType)){
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
		}
		$this->data['genresType'] = $gentres;
		/** get gentres type list end*/
		
		/** get mood type list */
		$moodType	=	$this->Common_model->getMoodType( );
		$mood['']='Select type';
		if(!empty($moodType)){
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
		}
		$this->data['moodType'] = $mood;
		/** get mood type list  end */
		
	/*	if (!empty($_POST)) {
			$searchData	=	$this->Manager_model->manageSongSearchInfo( $_POST );
		
			if(!empty($searchData[0])){
				$this->data['search'] = $searchData;
			}
// 			 echo '<pre>';print_r($searchData);exit;
		}*/
		
		$this->load->view('layout/header');
		$this->load->view('song/view',$this->data);
		$this->load->view('layout/footer');
	}

	/**
	 * Ajax call using search album info
	 */
	public function search(){
		$this->load->model("song/Manager_model");
		/** set default value in ajax call DataTable boostrap */
		$resultset['recordsTotal'] = 0;
		$resultset['recordsFiltered'] = 0;
		$resultset['data'] = array();
	
		if (!empty($_POST)) {
			/** set limit in ajax call DataTable boostrap */
			$start=0;
			$end=LIMIT_DATATBLE;
			$length = (isset($_POST['start'])?$_POST['start']:0);
			if($length >= $end ){
				$start=$_POST['start'];
			}
	
			/** get artist searched data */
			$searchData	=	$this->Manager_model->manageSongSearchInfo( json_decode($_POST['data'],true), $start, $end );
	
			$info = base_url().'song/index/information/';
			$edit = base_url().'song/index/edit_songs/';
	
			if(!empty($searchData[0])){
				foreach ( $searchData as $val ){
					$url = '<a href="'.$info.$val['SONG_ID'].'" title="view song info"> <i class="fa fa-list-alt"></i></a>
							<a href="'.$edit.$val['SONG_ID'].'" title="edit song info"> <i class="fa fa-edit"></i></a>';
	
					$data[]=array($val['SONG_NAME'],$val['ALBUM_NAME'],$val['SONG_AMOUNT'],$val['SONG_COMPANY_PERCENTAGE'],$url);
				}
				/** set value in ajax call DataTable boostrap */
				$draw = (isset($_POST['draw'])?$_POST['draw']:1);
				$count = count($data);
				$resultset['draw'] = $draw;
				$resultset['recordsTotal'] = $start+$count;
				$resultset['recordsFiltered'] = $start+$count+1;
				$resultset['data'] = $data;
			}
	
		}
	
		echo json_encode($resultset);exit;
	}
	
	/**
	 * Song insertion action
	 * @return success and failure message
	 */
	function add(){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$message	=	'Song creation failed';
		$flag =0;
		/** cheacking post value */
		if( $this->input->post() ) {
			/** cheacking madatory fields */
			if (!empty($_POST['song_name']) && isset( $_FILES['song']['error'] ) && $_FILES['song']['error'] == 0 ){
				$songName	= $_POST['song_name'];
				$albumId	= (!empty($_POST['albumId'])?$_POST['albumId']:'');
				$genresType	= (!empty($_POST['genresType'])?$_POST['genresType']:0);
				$moodType	= (!empty($_POST['moodType'])?$_POST['moodType']:'');
				$song_amt	= (!empty($_POST['song_amt'])?$_POST['song_amt']:'');
				$percentage	= (!empty($_POST['percentage'])?$_POST['percentage']:'');
				$location	= (!empty($_POST['location'])?$_POST['location']:'');
				$country	= (!empty($_POST['country'])?$_POST['country']:'');
				$city		= (!empty($_POST['city'])?$_POST['city']:'');
				$podcast	= (!empty($_POST['podcast'])?1:0);
				$podcastType= (!empty($_POST['podcastType'])?$_POST['podcastType']:0);
				$songStatus= (!empty($_POST['song_status'])?$_POST['song_status']:0);

				/** checking podcast is enable then podcast category selected or not.if not selected podcast category
				 * insert process not doing and rediect to edit song pages
				 */
				if($podcast==1){
					if (empty( $podcastType )){
// 						$this->session->set_flashdata('message', 'please select podcast category');
						$this->session->set_flashdata('item', array('message' => 'please select podcast category','class' => 'failure'));
						redirect(base_url() . 'song/index');
					}
				}
				/** validate song name available or not */
//				$songExists	=	$this->Song_model->songDetail( $songName );//If you need enanble it

				/** validate song name details if username already avaliable then send error message otherwise do the process */
//				if(empty( $songExists )){ //If you need enanble it
					/** Logo image uploding */
					$data = array();
					$imageUpload = 1;
					$songUpload =1;
					$songPath = '';
					$songCoverImageName ='';
					$songUrlPath = '';
					$songUrlName ='';
					foreach ( $_FILES as $key=> $file ){
						switch ( $key ){
							case 'logo':
								$songPath = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/song/";
								$config['upload_path'] = $songPath;
								$config['allowed_types'] = 'jpg|png|jpeg|gif';
								$config['overwrite'] = TRUE;
								// 	        $config['max_size'] = '100';
								// 	        $config['max_width']  = '1024';
								// 	        $config['max_height']  = '768';
								$songCoverImageName = 'Song_'.strtotime(date(DATE_TIME_FORMAT));
								$config['file_name']  = $songCoverImageName;
								$this->load->library('upload', $config);
								$this->upload->initialize($config);
								if (!$this->upload->do_upload('logo')){
									$message =  $this->upload->display_errors();
									$imageUpload = 0;
								}else{
									$imageUploadInfo =$this->upload->data();
								}
								break;
							case 'song':
								/** song uploding */
								$config1 = array();
								$data1 = array();
								$songUrlPath = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_songs/";
								$config1['upload_path'] = $songUrlPath;
								$config1['allowed_types'] = '*';
								$config1['overwrite'] = TRUE;
								// 	        $config['max_size'] = '100';
								// 	        $config['max_width']  = '1024';
								// 	        $config['max_height']  = '768';
								$songUrlName = 'song_url_'.strtotime(date(DATE_TIME_FORMAT));
								$config1['file_name']  = $songUrlName;
								$this->load->library('upload', $config1);
								$this->upload->initialize($config1);
								if ( !$this->upload->do_upload('song')){
									$message =  $this->upload->display_errors();
									$songUpload =0;
								}else{
									$songUploadInfo =$this->upload->data();
								}
								break;
						}
					}
					/** checking file upload or not if file not upload send error message */
					if ( empty( $imageUpload ) || empty($songUpload)){
						if (!empty($songPath)){
							unlink($songPath.$songCoverImageName);
						}
						if (!empty($songUrlPath)){
							unlink($songUrlPath.$songCoverImageName);
						}
						/** failure activity entry start **/
						$activity = array(
								'action'	=>	ACTION_INSERT,
								'status' 	=>	ACTIVITY_STATUS_FAILURE,
								'input'		=>	json_encode($_FILES),
								'activity' 	=>	$class.' '.$message
						);
						$insertData	=	$this->Event_model->eventTracking( $activity );
						/** failure activity entry end **/
					}else{
						
						if(!empty( $imageUploadInfo['file_name'] ) ){
							/** set insert input */
							$created		=	date(DATE_TIME_FORMAT);
							$insertSongData =   array(
														'SONG_NAME'  			=>	$songName,
														'SONG_COVER_IMAGE'		=> $imageUploadInfo['file_name'],
														'SONG_URL'				=> $songUploadInfo['file_name'],
														'SONG_ALBUM_ID'			=>	$albumId,
														'SONG_COMPANY_PERCENTAGE'=>	$percentage,
														'SONG_AMOUNT' 			=>	$song_amt,
														'SONG_GENRES_ID'		=>	$genresType,
														'SONG_MOOD_ID' 			=>	$moodType,
														'SONG_LOCATION'			=>  $location,
														'SONG_CITY'      		=>  $city,
														'SONG_COUNTRY'   		=>  $country,
														'SONG_IS_PODCAST'   	=>  $podcast,
														'SONG_PODCAST_CATEGORY'	=>  $podcastType,
														'SONG_STATUS'			=>  $songStatus,
														'SONG_CREATED_ON'   	=>  $created,
														'SONG_UPDATED_ON'   	=>  $created
													);
							$insertUserId	    =  $this->Song_model->insertsong($insertSongData);
		
							/** song percentage table entry*/
							if (!empty( $insertUserId ) )   {
								/** song percentage table entry calculation start*/
								if (!empty( $_POST['artistType'])){
									$insertPercentageData = array();
									foreach ( $_POST['artistType'] as $key=>$val ){
										if (!empty( $_POST['artistList'][$val]) && !empty( $_POST['percenteage'][$val] )){
											$insertPercentageData[] =   array(
																		'SONG_ID'  				=>	$insertUserId,
																		'ARTIST_TYPE_ID'		=>  $val,
																		'ARTIST_ID'				=>	$_POST['artistList'][$val],
																		'ARTIST_PERCENTAGE'		=>	$_POST['percenteage'][$val],
																		'CREATED_ON'   			=>  $created,
																		'UPDATED_ON'   			=>  $created
																);
										}
									}
									$insertUserId	    =   $this->Song_model->insertSongPercentage($insertPercentageData);
								}
								/** song percentage table entry calculation end*/
								$message = "Registration Success";
								$flag =1;
							}else{
								$message = "Registration Failed";
							}
						}else{
							$message	='Image uploading failure please try agin';
						}
					}
			/*	}else{
					$message	=	'Song name already exists';
				}*/
			}else{
				$message	='Please fill mandatory fields';
			}
		}
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
		
			$flashClass = "success";
		}else{
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass = "failure";
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
	
// 		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		redirect(base_url() . 'song/index');
	}

	/**
	 * get artist name detais based on artist Type
	 * this function used for ajax call. 
	 * @param unknown $artistType
	 */
	public function getartist($artistType){
		$atrist1 = '';
		/** get artist name list */
		$artistDetails	=	$this->Artist_model->getArtistDetail('','','',$artistType );
		if(!empty($artistDetails)){
			$atrist1='<option value="">Select name</option>';
			foreach ($artistDetails as $id=>$val){
				$atrist1.='<option value="'.$val['ARTISTS_ID'].'">'.$val['ARTISTS_USERNAME'].'</option>';
			}
		}
		
		echo $atrist1;exit;
	}
	
	/**
	 * Edit song view page
	 * @param unknown $songId
	 */
	public function edit_songs( $songId ){
		
		$songExists = $this->Song_model->songDetail('', $songId);
		if (empty( $songExists[0] )){
			$this->session->set_flashdata('message', 'Invalid song Id');
			redirect(base_url().'song/index/view');
		}
		
		/** variable declaration*/
		$this->data		= '';
		
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
		
		/** get album name list */
		$albumDetails	=	$this->Album_model->albumDetail( );
		$album['']='Select name';
		if(!empty($albumDetails)){
			foreach ($albumDetails as $id=>$val){
				$album[$val['ALBUM_ID']]=$val['ALBUM_NAME'];
			}
		}
		$this->data['albumList'] = $album;
		/** get gentres type list */
		
		/** get gentres type list */
		$gentresType	=	$this->Common_model->getGenresType( );
		$gentres['']='Select type';
		if(!empty($gentresType)){
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
		}
		$this->data['genresType'] = $gentres;
		/** get gentres type list end*/
		
		/** get mood type list */
		$moodType	=	$this->Common_model->getMoodType( );
		$mood['']='Select type';
		if(!empty($moodType)){
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
		}
		$this->data['moodType'] = $mood;
		/** get mood type list  end */
		
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist[''] ='Select type';;
		if(!empty($type)){
			foreach ($type as $id=>$val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
		}
		$this->data['artistType'] = $atrist;
		/** get artist type list End */
		
		/** get Podcast type list */
		$Podcast	=	$this->Common_model->getPodcastType( );
		$PodcastType['']='Select type';
		if(!empty($Podcast)){
			foreach ($Podcast as $id=>$val){
				$PodcastType[$val['PODCAST_ID']]=$val['PODCAST_NAME'];
			}
		}
		$this->data['PodcastType'] = $PodcastType;
		/** get Podcast type list end*/

		/** get songs information */
		$this->data['songsData'] = $songExists[0];
		
		/** get songs percentage */
		$songPercentageExists = $this->Song_model->songPercentageDetail('', $songId);
		
		$this->data['songsPerData'] =  (!empty( $songPercentageExists )?$songPercentageExists:'');
		
		/** get artist name list */
		$artistDetails	=	$this->Artist_model->getArtistDetail( );
		if(!empty($artistDetails)){
			$this->data['artistList'] = $artistDetails;
		}
		/** get gentres type list */
		
		$this->load->view('layout/header');
		$this->load->view('song/edit', $this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Updating song information
	 * @param unknown $songId
	 */
	public function edit($songId){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$flag =0;
		/** checking songdetails based on songId. If not exits redirect view page */ 
		$songExists = $this->Song_model->songDetail('', $songId);
		if (empty( $songExists[0] )){
			$this->session->set_flashdata('message', 'Invalid song Id');
			redirect(base_url().'song/index/edit_songs');
		}
		
		$message =	'Update failed please try again';
		if (!empty($_POST['song_name']) && isset(  $_POST['old_song'] ) ){
			/** set updated podcast value */
			$podcast	= (!empty($_POST['podcast'])?1:0);
			$podcastType= (!empty($_POST['podcastType'])?$_POST['podcastType']:0);
			
			/** checking podcast is enable then podcast category selected or not.if not selected podcast category
			 * update process not doing and rediect to edit song pages
			 */
			if($podcast==1){
				if (empty( $podcastType )){
// 					$this->session->set_flashdata('message', 'please select podcast category');
					$this->session->set_flashdata('item', array('message' => 'please select podcast category','class' => 'failure'));
					redirect(base_url() . 'song/index/edit_songs/'.$songId);
				}
			}
				
			//If you need enanble it song name exits
			// 			$songname		= $_POST['song_name'];
			// 			$oldSongname	= (!empty($_POST['old_name'])?$_POST['old_name']:'');
		
			// 			$songExists = '';
		
			/** validate song name available or not */
			// 			if($songname	!= $oldSongname){
			//				$songExists	=	$this->Song_model->songDetail( $songname );//If you need enanble it
			// 			}
		
			/** validate album name details if username already avaliable then send error message otherwise do the process */
			//				if(empty( $songExists )){ //If you need enanble it
		
			/** image upload **/
			$song = $_POST['old_song'];
			/** checking image uploaded or not */
			if ( isset( $_FILES['song']['error'] ) && $_FILES['song']['error'] == 0) {
				$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_songs/";
				$config['allowed_types'] = '*';
				// 	        $config['max_size'] = '100';
				// 	        $config['max_width']  = '1024';
				// 	        $config['max_height']  = '768';
				$config['file_name']  = 'song_url_'.strtotime(date(DATE_TIME_FORMAT));
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload('song')){
					$message =  $this->upload->display_errors();
					/** failure activity entry start **/
					$activity = array(
							'action'	=>	ACTION_UPDATE,
							'status' 	=>	ACTIVITY_STATUS_FAILURE,
							'input'		=>	json_encode($_FILES),
							'activity' 	=>	$class.' '.$message
					);
					$insertData	=	$this->Event_model->eventTracking( $activity );
					/** failure activity entry end **/
				}else{
					$data = array('upload_data' => $this->upload->data());
				}
				$song = (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
			}
		
			/** get input modified data list */
			$songInfo		= $this->Song_model->setUpdateInfo($_POST);
		
			/** assign table format value */
// 			$songInfo['SONG_COVER_IMAGE']		= $logo;
			$songInfo['SONG_URL']				= $song;
			$songInfo['SONG_IS_PODCAST']		= $podcast;
			$songInfo['SONG_PODCAST_CATEGORY']	= $podcastType;
			$songInfo['SONG_UPDATED_ON']		= date(DATE_TIME_FORMAT);
		
			if(!empty( $songInfo )){
				/** update song table information*/
				$updateSong	    =   $this->Song_model->updateSong($songInfo, $songId);
				
				/** song percentage table update and insert calculation start */
				if (!empty( $_POST['artistType'])){
					/** declare variable*/
					$insertPercentageData = array();
					$updatePercentageData = array();
					$perId	= array();
					
					/** set insert and update value */
					foreach ( $_POST['artistType'] as $key=>$val ){
						if (!empty( $_POST['artistList'][$val]) && !empty( $_POST['percenteage'][$val] )){
							$percentageDetails = $this->Song_model->songPercentageDetail( '', $songId, $val );
							if (!empty($percentageDetails[0]['PERCENTAGE_ID'])){
								$updatePercentageData =   array(
																	'SONG_ID'				=>	$songId,
																	'ARTIST_TYPE_ID'		=>  $val,
																	'ARTIST_ID'				=>	$_POST['artistList'][$val],
																	'ARTIST_PERCENTAGE'		=>	$_POST['percenteage'][$val],
																	'UPDATED_ON'   			=>  date(DATE_TIME_FORMAT)
															);
								$Update	    =   $this->Song_model->updateSongPercentage($updatePercentageData, $percentageDetails[0]['PERCENTAGE_ID']);
								$message = "Update Success";
								$flag =1;
							}else{
								$insertPercentageData[] =   array(
																	'SONG_ID'				=>	$songId,
																	'ARTIST_TYPE_ID'		=>  $val,
																	'ARTIST_ID'				=>	$_POST['artistList'][$val],
																	'ARTIST_PERCENTAGE'		=>	$_POST['percenteage'][$val],
																	'CREATED_ON'   			=>  date(DATE_TIME_FORMAT),
																	'UPDATED_ON'   			=>  date(DATE_TIME_FORMAT)
															);
							}
						}
					}
					if (!empty( $insertPercentageData)){
						$insertUserId	    =   $this->Song_model->insertSongPercentage($insertPercentageData);
						$message = "Update Success";
						$flag =1;
					}
					/** song percentage calculation end**/
				}
			}else{
				$message = "Update Failed";
			}
			// 			}else{
			// 				$message = 'Song name already exists';
			// 			}
		}else{
			$message	='Please fill mandatory fields';
		}
		
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass = "success";
		
		}else{
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass = "failure";
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
// 		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		redirect(base_url().'song/index/edit_songs/'.$songId);
	}
	
	/**
	 * song information view based on song id
	 * @param unknown $songId
	 */
	function information($songId){
		/** checking input empty or not if empty then redirect view page**/
		if (empty( $songId )){
			$this->session->set_flashdata('message', 'Invalid song Id');
			redirect(base_url().'song/index/view');
		}
		
		/** get songs information based on songid */
		$request['songId'] = $songId;
		$info =  $this->Manager_model->manageSongSearchInfo($request);
		$this->data['songData'] =  (!empty( $info[0] )?$info[0]:'');
		
		$this->load->view('layout/header');
		$this->load->view('song/info', $this->data);
		$this->load->view('layout/footer');
// 		echo '<pre>';print_r($info);exit;
	}
 
}
 
?>