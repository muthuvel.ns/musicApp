<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Index extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('dashboard/Manager_model');
		
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	function index() {
		$this->data ='';
		$res = $this->Manager_model->manageDashboardInfo();
		if (!empty( $res )){
			$this->data = $res;
		}
	/*	$res1 = $this->Manager_model->manageSongInfo();
// 		echo '<pre>';print_r($res1);exit;
		$data = array();
		if (!empty($res1)){
			foreach ( $res1 as $val){
				$art = (!empty($val['ARTISTS_USERNAME'])?$val['ARTISTS_USERNAME']:'');
				$song = (!empty($val['SONG_NAME'])?$val['SONG_NAME']:'');
				$album = (!empty($val['ALBUM_NAME'])?$val['ALBUM_NAME']:'');
				$count = (!empty($val['SONG_LISTENED_COUNT'])?$val['SONG_LISTENED_COUNT']:0);
				$data[] =array('ARTISTS_USERNAME'=>$art, 'SONG_NAME'=>$song,'ALBUM_NAME'=>$album,'COUNT'=>$count);
			}
			$this->data['recent'] = $data;
		}*/
		
		$this->load->view('layout/header');
		$this->load->view('dashboard/index', $this->data);
		$this->load->view('layout/footer');
	}
	
	public function search(){
		$this->load->model("artist/Manager_model");
		/** set default value in ajax call DataTable boostrap */
		$resultset['recordsTotal'] = 0;
		$resultset['recordsFiltered'] = 0;
		$resultset['data'] = array();
	
		if (!empty($_POST)) {
			/** set limit in ajax call DataTable boostrap */
			$start=0;
			$end=LIMIT_DATATBLE;
			if(isset($_POST['start']) >= $end ){
				$start=$_POST['start'];
			}
				
			/** get artist searched data */
			$searchData	=	$this->Manager_model->manageDashboardSearchInfo($start, $end);
	
			if(!empty($searchData[0])){
				foreach ( $searchData as $val ){
					$data[]=array($val['ALBUM_NAME'],$val['SONG_NAME'],$val['ARTISTS_USERNAME'],$val['SONG_LISTENED_COUNT']);
				}
// 				echo '<pre>';print_r($data);exit;
				/** set value in ajax call DataTable boostrap */
				$draw = (isset($_POST['draw'])?$_POST['draw']:1);
				$count = count($data);
				$resultset['draw'] = $draw;
				$resultset['recordsTotal'] = $start+$count;
				$resultset['recordsFiltered'] = $start+$count+1;
				$resultset['data'] = $data;
			}
		}
		echo json_encode($resultset);exit;
	}
	
	
}
 
?>