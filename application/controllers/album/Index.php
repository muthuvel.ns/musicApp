<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Index extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model("common/Common_model");
		$this->load->model("artist/Artist_model");
		$this->load->model("album/Album_model");
		$this->load->model("event_handler/Event_model");
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	/**
	 * Album creation Action
	 */
	function index() {
		/** declare variable */
		$this->data = '';
		
// 		$this->session->set_flashdata('item', array('message' => 'Record updated successfully','class' => 'success'));
		
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
		
		/** get artist name list */
		$atrist['']='Select name';
		$artistDetails	=	$this->Artist_model->getArtistDetail( );
		if(!empty($artistDetails)){
			foreach ($artistDetails as $id=>$val){
				$atrist[$val['ARTISTS_ID']]=$val['ARTISTS_USERNAME'];
			}
		}
		$this->data['artistList'] = $atrist;
		/** get artist list end*/
		
		/** get gentres type list */
		$gentres['']='Select type';
		$gentresType	=	$this->Common_model->getGenresType( );
		if(!empty($gentresType)){
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
		}
		$this->data['genresType'] = $gentres;
		/** get gentres type list end*/
		
		/** get mood type list */
		$mood['']='Select type';
		$moodType	=	$this->Common_model->getMoodType( );
		if(!empty($moodType)){
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
		}
		$this->data['moodType'] = $mood;
		/** get mood type list  end */
		
		/** get Podcast type list */
		$Podcast	=	$this->Common_model->getPodcastType( );
		$PodcastType['']='Select type';
		if(!empty($Podcast)){
			foreach ($Podcast as $id=>$val){
				$PodcastType[$val['PODCAST_ID']]=$val['PODCAST_NAME'];
			}
		}
		$this->data['PodcastType'] = $PodcastType;
		/** get Podcast type list end*/
		
// 		echo '<pre>';print_r($this->data);exit;
		$this->load->view('layout/header');
		$this->load->view('album/create',$this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Album view and search action
	 */
	function view() {
		$this->load->model("album/Manager_model");
		/** variable declaration*/
		$this->data = '';
		/** get artist name list */
		$atrist['']='Select name';
		$artistDetails	=	$this->Artist_model->getArtistDetail( );
		if(!empty($artistDetails)){
			foreach ($artistDetails as $id=>$val){
				$atrist[$val['ARTISTS_ID']]=$val['ARTISTS_USERNAME'];
			}
		}
		$this->data['artistList'] = $atrist;
		/** get artist list end */
		
		/** get gentres type list */
		$gentres['']='Select type';
		$gentresType	=	$this->Common_model->getGenresType( );
		if(!empty($gentresType)){
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
		}
		$this->data['genresType'] = $gentres;
		/** get gentres type list end*/
		
		/** get mood type list */
		$mood['']='Select type';
		$moodType	=	$this->Common_model->getMoodType( );
		if(!empty($moodType)){
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
		}
		$this->data['moodType'] = $mood;
		/** get mood type list  end */ 

		/** get album searching data based on post value */
		/*if (!empty($_POST)) {
			$searchData	=	$this->Manager_model->manageAlbumSearchInfo( $_POST );
			if(!empty($searchData[0])){
				$this->data['search'] = $searchData;
			}
		}*/

		$this->load->view('layout/header');
		$this->load->view('album/view', $this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Ajax call using search album info
	 */
	public function search(){
		$this->load->model("album/Manager_model");
		/** set default value in ajax call DataTable boostrap */
		$resultset['recordsTotal'] = 0;
		$resultset['recordsFiltered'] = 0;
		$resultset['data'] = array();
	
		if (!empty($_POST)) {
			/** set limit in ajax call DataTable boostrap */
			$start=0;
			$end=LIMIT_DATATBLE;
			$length = (isset($_POST['start'])?$_POST['start']:0);
			if($length >= $end ){
				$start=$_POST['start'];
			}
				
			/** get artist searched data */
			$searchData	=	$this->Manager_model->manageAlbumSearchInfo( json_decode($_POST['data'],true), $start, $end );

			$info = base_url().'album/index/information/';
			$edit = base_url().'album/index/edit_album/';
				
			if(!empty($searchData[0])){
				foreach ( $searchData as $val ){
					$url = '<a href="'.$info.$val['ALBUM_ID'].'" title="view album info"> <i class="fa fa-list-alt"></i></a>
							<a href="'.$edit.$val['ALBUM_ID'].'" title="edit album info"> <i class="fa fa-edit"></i></a>';
					
					$image = '';
					if (!empty($val['ALBUM_LOGO'])){
						$image ='<img alt="LOGO" width="50" height="50" src="'. base_url().'assets/upload_images/album/'.$val['ALBUM_LOGO'].'" >';
					}
					$status = 'Inactive';
					if (isset($val['ALBUM_STATUS']) && $val['ALBUM_STATUS']==1){
						$status = 'Active';
					}
					
						
					$data[]=array($val['ALBUM_NAME'],$val['SONG_COUNT'],$image, $status,$url);
				}
				/** set value in ajax call DataTable boostrap */
				$draw = (isset($_POST['draw'])?$_POST['draw']:1);
				$count = count($data);
				$resultset['draw'] = $draw;
				$resultset['recordsTotal'] = $start+$count;
				$resultset['recordsFiltered'] = $start+$count+1;
				$resultset['data'] = $data;
			}
				
		}
	
		echo json_encode($resultset);exit;
	}
	
	/**
	 * Album insertion action
	 */
	function add(){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$message	=	'Album creation failed';
		$flag =0;
		/** checking post value */
		if( $this->input->post() ) {
			/** chacking madatory fields */
			if (!empty($_POST['album_name']) && isset( $_FILES['logo']['error'] ) && $_FILES['logo']['error'] == 0 ){
				$albumName	= $_POST['album_name'];
				$artistId	= (!empty($_POST['artistId'])?$_POST['artistId']:'');
				$genresType	= (!empty($_POST['genresType'])?$_POST['genresType']:'');
				$moodType	= (!empty($_POST['moodType'])?$_POST['moodType']:'');
				$location	= (!empty($_POST['location'])?$_POST['location']:'');
				$country	= (!empty($_POST['country'])?$_POST['country']:'');
				$city		= (!empty($_POST['city'])?$_POST['city']:'');
				$podcast	= (!empty($_POST['podcast'])?1:0);
				$podcastType= (!empty($_POST['podcastType'])?$_POST['podcastType']:0);
				$albumStatus= (!empty($_POST['album_status'])?$_POST['album_status']:0);

				/** checking podcast is enable then podcast category selected or not.if not selected podcast category
				 * insert process not doing and rediect to edit song pages
				 */
				if($podcast==1){
					if (empty( $podcastType )){
						$this->session->set_flashdata('item', array('message' => 'please select podcast category','class' => 'failure'));
						redirect(base_url() . 'album/index');
					}
				}
				/** validate album name available or not */
//				$albumExists	=	$this->Album_model->albumDetail( $albumName );//If you need enanble it

				/** validate album name details if username already avaliable then send error message otherwise do the process */
//				if(empty( $albumExists )){ //If you need enanble it
					$data = array();
					$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/album/";
					$config['allowed_types'] = '*';
					// 	        $config['max_size'] = '100';
					// 	        $config['max_width']  = '1024';
					// 	        $config['max_height']  = '768';
					$config['file_name']  = 'Album_'.strtotime(date(DATE_TIME_FORMAT));
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload('logo')){
						$message =  $this->upload->display_errors();
						/** failure activity entry start **/
						$activity = array(
								'action'	=>	ACTION_INSERT,
								'status' 	=>	ACTIVITY_STATUS_FAILURE,
								'input'		=>	json_encode($_FILES),
								'activity' 	=>	$class.' message=>'.$message
						);
						$insertData	=	$this->Event_model->eventTracking( $activity );
						/** failure activity entry end **/
					}else{
						$data = array('upload_data' => $this->upload->data());
						if(!empty( $data['upload_data']['file_name'] ) ){
							/** set insert inputs*/
							$created		=	date(DATE_TIME_FORMAT);
							$insertAlbumData =   array(
														'ALBUM_NAME'  			=>	$albumName,
														'ALBUM_LOGO'			=>  $data['upload_data']['file_name'],
														'ALBUM_ARTISTS_ID'		=>	$artistId,
														'ALBUM_GENRES_ID'		=>	$genresType,
														'ALBUM_MOOD_ID' 		=>	$moodType,
														'ALBUM_LOCATION'		=>  $location,
														'ALBUM_CITY'      		=>  $city,
														'ALBUM_COUNTRY'   		=>  $country,
														'ALBUM_IS_PODCAST'   	=>  $podcast,
														'ALUBM_PODCAST_CATEGORY'=>  $podcastType,
														'ALBUM_STATUS'			=>  $albumStatus,
														'ALBUM_CREATED_ON'   	=>  $created,
														'ALBUM_UPDATED_ON'   	=>  $created
													);
							/** album nsertion based on insertion data */
							$insertUserId	    =   $this->Album_model->insertAlbum($insertAlbumData);
		
							if (!empty( $insertUserId ) )   {
								$message = "Registration Success";
								$flag =1;
							}else{
								$message = "Registration Failed";
							}
						}else{
							$message	='Image uploading failure please try agin';
						}
					}
			/*	}else{
					$message	=	'Album name already exists';
				}*/
			}else{
				$message	='Please fill mandatory fields';
			}
		}
	
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' message=>'.$message
			);
			$flashClass = 'success';
		}else{
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' message=>'.$message
			);
			$flashClass = 'failure';
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		
	//	$this->session->set_flashdata('message', $message);
		redirect(base_url() . 'album/index');
	}
	
	/**
	 * edit view page based on albumId
	 * @param unknown $albumId
	 */
	function edit_album( $albumId ) {
		/** variable declaration*/
		$this->data		= '';
	
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
	
		/** get artist name list */
		$artistDetails	=	$this->Artist_model->getArtistDetail( );
		if(!empty($artistDetails)){
			$atrist['']='Select name';
			foreach ($artistDetails as $id=>$val){
				$atrist[$val['ARTISTS_ID']]=$val['ARTISTS_USERNAME'];
			}
			$this->data['artistList'] = $atrist;
		}
		/** get artist list end*/
	
		/** get gentres type list */
		$gentresType	=	$this->Common_model->getGenresType( );
		if(!empty($gentresType)){
			$gentres['']='Select type';
			foreach ($gentresType as $id=>$val1){
				$gentres[$val1['GENRES_ID']]=$val1['GENRES_NAME'];
			}
			$this->data['genresType'] = $gentres;
		}
		/** get gentres type list end*/
	
		/** get mood type list */
		$moodType	=	$this->Common_model->getMoodType( );
		if(!empty($moodType)){
			$mood['']='Select type';
			foreach ($moodType as $id=>$val2){
				$mood[$val2['MOOD_ID']]=$val2['MOOD_NAME'];
			}
			$this->data['moodType'] = $mood;
		}
		/** get mood type list  end */
	
		/** get Podcast type list */
		$Podcast	=	$this->Common_model->getPodcastType( );
		$PodcastType = array();
		if(!empty($Podcast)){
			$PodcastType['']='Select type';
			foreach ($Podcast as $id=>$val){
				$PodcastType[$val['PODCAST_ID']]=$val['PODCAST_NAME'];
			}
			$this->data['PodcastType'] = $PodcastType;
		}
		/** get Podcast type list end*/
		
		/** get album details based on albumId */
		$albumDetails = $this->Album_model->albumDetail('', $albumId);
		if (!empty( $albumDetails[0])){
			$this->data['albumInfo'] = $albumDetails[0];
		}

		$this->load->view('layout/header');
		$this->load->view('album/edit',$this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Update Action
	 * update album information based on post values
	 * @param unknown $albumId
	 */
	public function edit($albumId){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$this->data = '';
		$flag =0;
		/** checking albumdetails based on songId. If not exits redirect view page */
		$albumExists	=	$this->Album_model->albumDetail( '', $albumId );
		if(empty($albumExists[0])){
			$this->session->set_flashdata('message', 'Invalid album information');
			redirect(base_url() . 'album/index/view'); //@todo need to view page url
		}
		
		$message =	'Update failed please try again';
		if (!empty($_POST['album_name']) && isset(  $_POST['old_logo'] ) && !empty( $_POST['old_logo']) ){
			/** set updated podcast value */
			$podcast	= (!empty($_POST['podcast'])?1:0);
			$podcastType= (!empty($_POST['podcastType'])?$_POST['podcastType']:0);
			
			/** checking podcast is enable then podcast category selected or not.if not selected podcast category
			 * update process not doing and rediect to edit song pages
			 */
			if($podcast==1){
				if (empty( $podcastType )){
					$this->session->set_flashdata('item', array('message' => 'please select podcast category','class' => 'failure'));
					redirect(base_url() . 'album/index/edit_album/'.$albumId);
				}
			}
			
			//If you need enanble it album name exits
// 			$albumname		= $_POST['album_name'];
// 			$olduAlbumname	= (!empty($_POST['old_name'])?$_POST['old_name']:'');
		
// 			$albumExists = '';
		
			/** validate album name available or not */
// 			if($albumname	!= $olduAlbumname){
//				$albumExists	=	$this->Album_model->albumDetail( $albumName );//If you need enanble it
// 			}

				/** validate album name details if username already avaliable then send error message otherwise do the process */
//				if(empty( $albumExists )){ //If you need enanble it
						
					/** image upload **/
					$logo = $_POST['old_logo'];
					if ( isset( $_FILES['logo']['error'] ) && $_FILES['logo']['error'] == 0) {
						$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/album/";
						$config['allowed_types'] = '*';
						// 	        $config['max_size'] = '100';
						// 	        $config['max_width']  = '1024';
						// 	        $config['max_height']  = '768';
						$config['file_name']  = 'Album_'.strtotime(date(DATE_TIME_FORMAT));
						$this->load->library('upload', $config);
						if ( ! $this->upload->do_upload('logo')){
							$message =  $this->upload->display_errors();
								/** failure activity entry start **/
								$activity = array(
										'action'	=>	ACTION_UPDATE,
										'status' 	=>	ACTIVITY_STATUS_FAILURE,
										'input'		=>	json_encode($_FILES),
										'activity' 	=>	$class.''.$message
								);
								$insertData	=	$this->Event_model->eventTracking( $activity );
								/** failure activity entry end **/
						}else{
							$data = array('upload_data' => $this->upload->data());
						}
						$logo = (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
					}
		
					$updateAlbum 	= '';
					/** get input modified data list */
					$albumInfo		= $this->Album_model->setUpdateInfo($_POST);

					/** assign table format value */
					$albumInfo['ALBUM_LOGO']				= $logo;
					$albumInfo['ALBUM_IS_PODCAST']			= $podcast;
					$albumInfo['ALUBM_PODCAST_CATEGORY']	= $podcastType;
					$albumInfo['ALBUM_UPDATED_ON']			= date(DATE_TIME_FORMAT);
						
					if(!empty( $albumInfo )){
						$updateAlbum	    =   $this->Album_model->updateAlbum($albumInfo, $albumId);
					}
						
					if (!empty( $updateAlbum )) {
						$flag =1;
						$message = "Update Success";
					}else{
						$message = "Update Failed";
					}
// 			}else{
// 				$message = 'Album name already exists';
// 			}
		}else{
			$message	='Please fill mandatory fields';
		}
		
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.''.$message
			);
			$flashClass = 'success';
		
		}else{
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.''.$message
			);
			$flashClass = 'failure';
		}
		
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		//$this->session->set_flashdata('message', $message);
		redirect(base_url() . 'album/index/edit_album/'.$albumId);
	}
	
	/**
	 * Album inforamtion
	 * @param unknown $albumId
	 */
	function information( $albumId ) {
		/** checking input empty or not if empty then redirect view page**/
		if (empty( $albumId )){
			$this->session->set_flashdata('message', 'Invalid album Id');
			redirect(base_url().'album/index/view');
		}
		
		/** variable declaration*/
		$this->data ='';
		
		/** get album information based on albumId*/
		$albumDetails = $this->Album_model->getAlbumDetail('', $albumId);
		if (!empty( $albumDetails[0])){
			$this->data['albumInfo'] = $albumDetails[0];
		}

		$this->load->view('layout/header');
		$this->load->view('album/info',$this->data);
		$this->load->view('layout/footer');
	}
 
}
 
?>