<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Login extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->helper('captcha');
		$this->load->library('form_validation');
		$this->load->model("login/Login_model");
		$this->load->model("event_handler/Event_model");
		
		if( isset($this->session->userdata['ADMIN_ID']) || !empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url().'dashboard/index');
		}
		
	}
 
	function index() {
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		
		//$this->data['captcha'] ='';
		//$this->data['message'] = $this->session->flashdata('message');
		
		/** validating form fields */
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('userCaptcha', 'Captcha', 'required|callback_check_captcha');
		$userCaptcha = $this->input->post('userCaptcha');
		if ($this->form_validation->run() == false){
			$this->captcha_setting();
		}else {
			
			$this->data['message']	=	'Username or Password invalid';
			
			if( $this->input->post() ) {
				$formvalues	=	$this->input->post();
				$userdata	=	$this->Login_model->loginValidation( $formvalues );
				if ( !empty( $userdata ) )	{
					/** check email count is one then redirect to crossponding user domain*/
					if( count($userdata) == 1 ) {/** User has one role */
						/** Get user name  */
						$firstName	= ucfirst($userdata['0']['FIRSTNAME']);
						$lastName	= ucfirst($userdata['0']['LASTNAME']);
						$userName	= ucfirst($userdata['0']['USERNAME']);
						$userId		= $userdata['0']['ADMIN_USER_ID'];
							
						/** Set session value  */
						$data['ADMIN_ID']		=  $userdata['0']['ADMIN_USER_ID'];
						$data['email']			= $userdata['0']['EMAIL'];
						$data['username']		= $userName;
						$data['fname']			= $firstName;
						$data['lname']			= $lastName;
							
						$this->session->set_userdata($data);
			
						/** Login activity entry start **/
						$activity = array(
											'action'	=>	ACTION_LOGIN,
											'input'		=>	json_encode($data),
											'status' 	=>	ACTIVITY_STATUS_SUCCESS,
											'userid' 	=>	$userdata['0']['ADMIN_USER_ID']
										);
			
						$insertData	=	$this->Event_model->eventTracking( $activity );
						/** Login activity entry end **/
			
						redirect(base_url().'dashboard/index');
					}
				}else{
					/** Login failure activity entry start **/
					$activity = array(
										'action'	=>	ACTION_LOGIN,
										'status' 	=>	ACTIVITY_STATUS_FAILURE,
										'input'		=>	json_encode($formvalues),
										'activity'	=>	$class
									);
						
					$insertData	=	$this->Event_model->eventTracking( $activity );
						
					/** Login failure activity entry end **/
						
					$this->data['message']	=	'Username or Password you entered is incorrect';
				}
			}
			
			$this->captcha_setting();
			
			/*if ($this->form_validation->run() == true && !empty( $this->data['message'] )) {//echo '<pre>';print_r('2');exit;

				$random_number = substr(number_format(time() * rand(),0,'',''),0,6);
				$vals = array(
								'word' => $random_number,
								'img_path' => './assets/captcha/',
								'img_url' => base_url().'assets/captcha/',
								'img_width' =>285,
								'img_height' => 40,
								'expiration' => 7200
							);
					
				$this->data['captcha'] = create_captcha($vals);
				$this->session->set_userdata('captchaWord',$this->data['captcha']['word']);
			}*/
			
		}
		
	}
	
	/**
	 * @TODO This function now not used maybe used future
	 */
// 	public function loginprocess(){
		
// 		$message	=	'Username or Password invalid';
		
// 		if( $this->input->post() ) {
// 				$formvalues	=	$this->input->post();
// 				$userdata	=	$this->Login_model->loginValidation( $formvalues );
// 				if ( !empty( $userdata ) )	{
// 					/** check email count is one then redirect to crossponding user domain*/
// 					if( count($userdata) == 1 ) {/** User has one role */
// 						/** Get user name  */
// 						$firstName	= ucfirst($userdata['0']['FIRSTNAME']);
// 						$lastName	= ucfirst($userdata['0']['LASTNAME']);
// 						$userName	= ucfirst($userdata['0']['USERNAME']);
// 						$userId		= $userdata['0']['ADMIN_USER_ID'];
			
// 						/** Set session value  */ 
// 						$data['ADMIN_ID']		=  $userdata['0']['ADMIN_USER_ID'];
// 						$data['email']			= $userdata['0']['EMAIL'];
// 						$data['username']		= $userName;
// 						$data['fname']			= $firstName;
// 						$data['lname']			= $lastName;
			
// 						$this->session->set_userdata($data);
						
// 						/** Login activity entry start **/
// 						$activity = array( 
// 											'userid' 	=>	$userdata['0']['ADMIN_USER_ID'],
// 											'status' 	=>	'success',
// 											'activity1'	=>	json_encode($data)
// 						);
						
// 						//$insertData	=	$this->Event_model->eventTracking( $activity );
// 						/** Login activity entry end **/
						
// 						redirect(base_url().'dashboard/index');
// 					}
// 				}else{
// 					/** Login failure activity entry start **/
// 					$activity = array(
// 										'userid' 	=>	'invaild',
// 										'status' 	=>	'failure',
// 										'activity1'	=>	json_encode($formvalues)
// 								);
					
// 					//$insertData	=	$this->Event_model->eventTracking( $activity );
					
// 					/** Login failure activity entry end **/
					
// 					$message	=	'Username or Password you entered is incorrect';
// 				}
// 		}
		
// 		$this->session->set_flashdata('message', $message); 	        
// 		$this->index();
// 	}
	
	/**
	 * validate captcha
	 * @param unknown $str
	 * @return boolean
	 */
	public function check_captcha($str){
		$word = $this->session->userdata('captchaWord');
		if(strcmp(strtoupper($str),strtoupper($word)) == 0){
			return true;
		}
		else{
			$this->form_validation->set_message('check_captcha', 'Invalid captcha!');
			return false;
		}
	}
	
	// This function genrate CAPTCHA image and store in "image folder".
	public function captcha_setting(){
		/** numeric random number for captcha*/
		$random_number = substr(number_format(time() * rand(),0,'',''),0,6);
		/** setting up captcha config*/
		$vals = array(
				'word' => $random_number,
				'img_path' => './assets/captcha/',
				'img_url' => base_url().'assets/captcha/',
				'img_width' =>245,
				'img_height' => 40,
				'expiration' => 7200
		);
			
		$this->data['captcha'] = create_captcha($vals);
		$this->session->set_userdata('captchaWord',$this->data['captcha']['word']);
		
		$this->load->view('login_layout/header');
		$this->load->view('login/index', $this->data);
		$this->load->view('login_layout/footer');
	}
	
	// For new image on click refresh button.
	public function captcha_refresh(){
		/** numeric random number for captcha*/
		$random_number = substr(number_format(time() * rand(),0,'',''),0,6);
		/** setting up captcha config*/
		$vals = array(
				'word' => $random_number,
				'img_path' => './assets/captcha/',
				'img_url' => base_url().'assets/captcha/',
				'img_width' =>245,
				'img_height' => 40,
				'expiration' => 7200
		);
			
		$data['captcha'] = create_captcha($vals);
		
		$this->session->set_userdata('captchaWord',$data['captcha']['word']);
		$respose['image'] = $data['captcha']['image'];
		echo json_encode($respose);
	
	}
 
}


 
?>