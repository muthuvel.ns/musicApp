<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Account extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model("user/Manager_model");
		$this->load->model("user/User_model");
		$this->load->model("event_handler/Event_model");
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	/**
	 * view and search page
	 */
	function index() {
		/** variable declaration */
		$this->data ='';
		
		/** get user inforamtion based on inputs**/
		/*if (!empty($_POST)) {
			$searchData	=	$this->Manager_model->manageUserSearchInfo( $_POST );
			if(!empty($searchData[0])){
				$this->data['search'] = $searchData;
			}
		}*/
		
		$this->load->view('layout/header');
		$this->load->view('user/index', $this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Ajax call using search album info
	 */
	public function search(){
		$this->load->model("user/Manager_model");
		/** set default value in ajax call DataTable boostrap */
		$resultset['recordsTotal'] = 0;
		$resultset['recordsFiltered'] = 0;
		$resultset['data'] = array();
	
		if (!empty($_POST)) {
			/** set limit in ajax call DataTable boostrap */
			$start=0;
			$end=LIMIT_DATATBLE;
			$length = (isset($_POST['start'])?$_POST['start']:0);
			if($length >= $end ){
				$start=$_POST['start'];
			}
	
			/** get artist searched data */
			$searchData	=	$this->Manager_model->manageUserSearchInfo( json_decode($_POST['data'], true), $start, $end );
	
			$info = base_url().'user/account/information/';
			$edit = base_url().'user/account/edit_user/';
	
			if(!empty($searchData[0])){
				foreach ( $searchData as $val ){
					$url = '<a href="'.$info.$val['USER_ID'].'" title="view user info"> <i class="fa fa-list-alt"></i></a>
							<a href="'.$edit.$val['USER_ID'].'" title="edit user info"> <i class="fa fa-edit"></i></a>';
					
					$gender = 'Others';
					if (isset($val['USER_GENDER']) && $val['USER_GENDER']==1){
						$gender = 'Male';
					}elseif (isset($val['USER_GENDER']) && $val['USER_GENDER']==2){
						$gender = 'Female';
					}
					
					$status = 'Inactive';
					if (isset($val['USER_STATUS']) && $val['USER_STATUS']==1){
						$status = 'Active';
					}
					
// 					$gender = (!empty(constant("GENDER_".$val['USER_GENDER']))? constant("GENDER_".$val['USER_GENDER']):'');
// 					$status = (!empty(constant("STATUS_".$val['USER_STATUS']))?constant("STATUS_".$val['USER_STATUS']):'');
	
					$data[]=array($val['USER_USERNAME'],$val['USER_EMAIL'],date(DOB_DATE_FORMAT,strtotime($val['USER_DOB'])),$gender,$status,$url);
				}
				/** set value in ajax call DataTable boostrap */
				$draw = (isset($_POST['draw'])?$_POST['draw']:1);
				$count = count($data);
				$resultset['draw'] =$draw;
				$resultset['recordsTotal'] = $start+$count;
				$resultset['recordsFiltered'] = $start+$count+1;
				$resultset['data'] = $data;
			}
	
		}
	
		echo json_encode($resultset);exit;
	}
	/**
	 * User information view page
	 * @param unknown $userId
	 */
	function information($userId){
		/** checking input empty or not if empty then redirect view page**/
		if (empty($userId)){
			redirect(base_url().'user/account/view');
		}
		
		$this->data = '';
		/** get user information based on userId */
// 		$request['userId'] = $userId;
		$searchData	=	$this->User_model->userDetail( '','', $userId );

		if(!empty($searchData[0])){
			$this->data['userData'] = $searchData[0];
		}
		
		$this->load->view('layout/header');
		$this->load->view('user/info', $this->data);
		$this->load->view('layout/footer');
	}
	
	function edit_user($userId){
		$this->data['flash'] = $this->session->flashdata('item');
		
		$userDetails	=	$this->User_model->userDetail( '','', $userId );
		if (!empty( $userDetails[0])){
			$this->data['userData'] = $userDetails[0];
		}
		
		$this->load->view('layout/header');
		$this->load->view('user/edit',$this->data);
		$this->load->view('layout/footer');
		
	}
	function edit($userId){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$this->data = '';
		
		$userIdExists	=	$this->User_model->userDetail( '','', $userId );
		if(empty($userIdExists[0])){
			$this->session->set_flashdata('message', 'Invalid album information');
			redirect(base_url() . 'artist/index/view');
		}
		$flag =0;
		$message =	'Update failed please try again';
		/** check mandatory fields*/
		if (!empty($_POST['email']) && !empty($_POST['username']) ){
			$email			= $_POST['email'];
			$oldEmail		= (!empty($_POST['old_email'])?$_POST['old_email']:'');
			$username		= $_POST['username'];
			$olduUsername	= (!empty($_POST['old_username'])?$_POST['old_username']:'');
		
			$userExists		= '';
			$usernameExists	= '';
			/** old email and new email are same then no need to check email exists or not */
			if($email	!= $oldEmail){
				$userExists	=	$this->User_model->userDetail( $email );
			}
				
			/** old username and new username are same then no need to check username exists or not */
			if($username	!= $olduUsername){
				$usernameExists	=	$this->User_model->userDetail( '', $username );
			}
			if ( empty( $usernameExists )) {
				if ( empty( $userExists )) {
						
					$updateUser 	= '';
					/** get input modified data list */
					$userInfo		= $this->User_model->setUpdateInfo($_POST);
						
					$created	=	date(DATE_TIME_FORMAT);
					$userInfo['USER_UPDATED_ON']		= $created;
						
					if(!empty( $userInfo )){
						$updateUser	    =   $this->User_model->updateUser($userInfo, $userId);
					}
						
					if (!empty( $updateUser )) {
						$message = "Update Success";
						$flag =1;
						//$insertData	=	$this->Event_model->insertAndUpdateEventTracking( $activity );
					}else{
						$message = "Update Failed";
						//$insertData	=	$this->Event_model->insertAndUpdateEventTracking( $activity );
					}
				}else{
					$message = 'User Already exists with same email';
				}
			}else{
				$message = 'User Already exists with same username';
			}
		}
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass ="success";
		}else{
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass ="failure";
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		redirect(base_url() . 'user/account/edit_user/'.$userId);
	}
 
	
	/**
	 * Check email avaliability using Ajax call
	 * @author
	 * @return integer value
	 */
	public function emailavailability(){
		if(empty($_POST['email'])){
			echo "3";exit;
		}
		$userExists	=	$this->User_model->userDetail( $_POST['email'] );
			
		$id = (!empty($_GET['id'])?$_GET['id']:''); //using edit function same user email can't change then send avaliable only other then send not available
		if ( !empty( $userExists ) && $userExists[0]['USER_ID'] == $id) {
			echo "2";
		}elseif ( !empty( $userExists )) {
			echo "1";
		}else{
			echo "2";
		}
		exit;
	}
	
	/**
	 * Check user avaliability using Ajax call
	 * @author
	 * @return integer value
	 */
	public function useravailability(){
		if(empty($_POST['uname'])){
			echo "3";exit;
		}
		$userExists	=	$this->User_model->userDetail( '', $_POST['uname'] );
			
		$id = (!empty($_GET['id'])?$_GET['id']:''); //using edit function same user email can't change then send avaliable only other then send not available
		if ( !empty( $userExists ) && $userExists[0]['USER_ID'] == $id) {
			echo "2";
		}elseif ( !empty( $userExists )) {
			echo "1";
		}else{
			echo "2";
		}
		exit;
	}
}
 
?>