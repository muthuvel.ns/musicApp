<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Index extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model("artist/Artist_model");
		$this->load->model("common/Common_model");
		$this->load->model("event_handler/Event_model");
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	/**
	 * Artist Creation 
	 * @author
	 */
	function index() {
		
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist['']='Select type';
		if(!empty($type)){
			foreach ($type as $id=>$val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
		}
		
		$this->data['artistType'] = $atrist;
		$this->load->view('layout/header');
		$this->load->view('artist/create', $this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Artist view and seach
	 * @author
	 */
	function view() {
		
		$this->load->model("artist/Manager_model");
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist = array();
		if(!empty($type)){
			$atrist['']='Select type';
			foreach ($type as $id=>$val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
		}
		
		$this->data['artistType'] = $atrist;
		if (!empty($_POST)) {
			$searchData	=	$this->Manager_model->manageArtistSearchInfo( $_POST );

			if(!empty($searchData[0])){
				$this->data['search'] = $searchData;
			}
		}
		$this->load->view('layout/header');
		$this->load->view('artist/view',$this->data);
		$this->load->view('layout/footer');
	}
	
	public function search(){
		$this->load->model("artist/Manager_model");
		/** set default value in ajax call DataTable boostrap */
		$resultset['recordsTotal'] = 0;
		$resultset['recordsFiltered'] = 0;
		$resultset['data'] = array();
		
		if (!empty($_POST)) {
			/** set limit in ajax call DataTable boostrap */
			$start=0;
			$end=LIMIT_DATATBLE;
			if(isset($_POST['start']) >= $end ){
				$start=$_POST['start'];
			} 
			
			/** get artist searched data */
			$searchData	=	$this->Manager_model->manageArtistSearchInfo( json_decode($_POST['data'],true), $start, $end );
		
			$info = base_url().'artist/index/information/';
			$edit = base_url().'artist/index/edit_artist/';
			
			if(!empty($searchData[0])){
				foreach ( $searchData as $val ){
					$url = '<a href="'.$info.$val['ARTISTS_ID'].'" title="view artist info"> <i class="fa fa-list-alt"></i></a>
							<a href="'.$edit.$val['ARTISTS_ID'].'" title="edit artist info"> <i class="fa fa-edit"></i></a>';
					
					$data[]=array($val['ARTISTS_TYPE_NAME'],$val['ARTISTS_USERNAME'],$val['ARTISTS_EMAIL'],$val['ARTISTS_PHONE'],0,$url);
				}
				/** set value in ajax call DataTable boostrap */
				$draw = (isset($_POST['draw'])?$_POST['draw']:1);
				$count = count($data);
				$resultset['draw'] = $draw;
				$resultset['recordsTotal'] = $start+$count;
				$resultset['recordsFiltered'] = $start+$count+1;
				$resultset['data'] = $data;
			}
		}
		echo json_encode($resultset);exit;
	}
	
	/**
	 * Artist insert based on inputs
	 * @author
	 */
	function add(){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$message	=	'Artist creation failed';
		$flag =0;
		if( $this->input->post() ) {
			/** check mandatory fields are avilable or not */ 
			if (!empty($_POST['email']) && !empty($_POST['artistType']) && !empty($_POST['username']) && !empty($_POST['password']) ){
				$email	= $_POST['email'];
				$pass	= $_POST['password'];
				$uname	= $_POST['username'];
				$artistType	= $_POST['artistType'];
				$mobile	= (!empty($_POST['mobile'])?$_POST['mobile']:'');
				$fname	= (!empty($_POST['fname'])?$_POST['fname']:'');
				$lname	= (!empty($_POST['lname'])?$_POST['lname']:'');
				$address= (!empty($_POST['location'])?$_POST['location']:'');
				$country= (!empty($_POST['country'])?$_POST['country']:'');
				$city	= (!empty($_POST['city'])?$_POST['city']:'');
				$status	= (!empty($_POST['artist_status'])?$_POST['artist_status']:'');
					
				/** validate username and email already available or not */ 
				$userExists	=	$this->Artist_model->artistDetail( $email );
				$usernameExists=	$this->Artist_model->artistDetail( '', $uname );

				/** validate username details if username already avaliable then send error message otherwise do the process */
				if(empty( $usernameExists )){
					/** validate email same as username validation */
					if ( empty( $userExists )) {
						$data = array();
						
						/** image file upload **/
						if ( isset( $_FILES['image']['error'] ) && $_FILES['image']['error'] == 0) {
							$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/artist/";
							$config['allowed_types'] = '*';
							// 	        $config['max_size'] = '100';
							// 	        $config['max_width']  = '1024';
							// 	        $config['max_height']  = '768';
							$config['file_name']  = 'Artist_'.strtotime(date(DATE_TIME_FORMAT));
							$this->load->library('upload', $config);
							if ( ! $this->upload->do_upload('image')){
								$message =  $this->upload->display_errors();
								/** failure activity entry start **/
								$activity = array(
										'action'	=>	ACTION_INSERT,
										'status' 	=>	ACTIVITY_STATUS_FAILURE,
										'input'		=>	json_encode($_FILES),
										'activity' 	=>	$class.' '.$message
								);
								$insertData	=	$this->Event_model->eventTracking( $activity );
								/** failure activity entry end **/
							}else{
								$data = array('upload_data' => $this->upload->data());
							}
						}
						
						/** set insertion input array **/
						$created		=	date(DATE_TIME_FORMAT);
						$insertUserData =   array(	
													'ARTISTS_TYPE_ID'   => $artistType,
													'ARTISTS_EMAIL'		=> $email,
													'ARTISTS_PASSWORD'	=> md5($pass),
													'ARTISTS_USERNAME'	=> $uname,
													'ARTISTS_PHONE' 	=> $mobile,
													'ARTISTS_IMAGE' 	=> (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:''),
													'ARTISTS_FIRSTNAME' =>  $fname,
								 					'ARTISTS_LASTNAME'  =>  $lname,
													'ARTISTS_LOCATION'	=>  $address,
													'ARTISTS_CITY'      =>  $city,
													'ARTISTS_COUNTRY'   =>  $country,
													'ARTISTS_STATUS'      =>  $status,
													'ARTISTS_CREATED_ON'   =>  $created,
													'ARTISTS_UPDATED_ON'   =>  $created
											);
						/** insert given input array */
						$insertUserId	    =   $this->Artist_model->insertArtist($insertUserData);
				
						if (!empty( $insertUserId ) )   {
							$message = "Registration Success";
							$flag =1;
						}else{
							$message = "Registration Failed";
				
						}
					}else{
						$message = 'User Already exists with same email';
					}
				}else{
					$message	=	'User Already exists with same username';
				}
			}
		}
		
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass ="success";
		}else{
			$activity = array(
					'action'	=>	ACTION_INSERT,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass ="failure";
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
		
// 		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		redirect(base_url() . 'artist/index');
	}
	
	/**
	 * Check email avaliability using Ajax call
	 * @author
	 * @return integer value
	 */
	public function emailavailability(){
		if(empty($_POST['email'])){
			echo "3";exit;
		}
		$userExists	=	$this->Artist_model->artistDetail( $_POST['email'] );
			
		$id = (!empty($_GET['id'])?$_GET['id']:''); //using edit function same user email can't change then send avaliable only other then send not available
		if ( !empty( $userExists ) && $userExists[0]['ARTISTS_ID'] == $id) {
			echo "2";
		}elseif ( !empty( $userExists )) {
			echo "1";
		}else{
			echo "2";
		}
		exit;
	}
	
	/**
	 * Check user avaliability using Ajax call
	 * @author
	 * @return integer value
	 */
	public function useravailability(){
		if(empty($_POST['uname'])){
			echo "3";exit;
		}
		$userExists	=	$this->Artist_model->artistDetail( '', $_POST['uname'] );
		
		$id = (!empty($_GET['id'])?$_GET['id']:''); //using edit function same user email can't change then send avaliable only other then send not available
		if ( !empty( $userExists ) && $userExists[0]['ARTISTS_ID'] == $id) {
			echo "2";
		}elseif ( !empty( $userExists )) {
			echo "1";
		}else{
			echo "2";
		}
		exit;
	}
	
	/**
	 * Edit view page
	 * Showing Artist information based on artistID
	 * @param unknown $artistId
	 * @author
	 */
	public function edit_artist( $artistId ){
		/** get flash message (error and success)*/
		$this->data['flash'] = $this->session->flashdata('item');
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist = array();
		if(!empty($type)){
			$atrist['']='Select type';
			foreach ($type as $val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
		}
		
		$this->data['artistType'] = $atrist;
		
		$artistDetails	=	$this->Artist_model->getArtistDetail( '','', $artistId );
		if (!empty( $artistDetails[0])){
			$this->data['artistData'] = $artistDetails[0];
		}
		
		$this->load->view('layout/header');
		$this->load->view('artist/edit',$this->data);
		$this->load->view('layout/footer');
	}
	
	/**
	 * Update artist Information based on artistId and Post value
	 * @param unknown $artistId
	 * @author
	 * @return sucess and failure message
	 */
	public function edit( $artistId ){
		$class= $this->router->fetch_class().'/'.$this->router->fetch_method();
		$this->data = '';
		$flag =0;
		$albumExists	=	$this->Artist_model->artistDetail( '','', $artistId );
		if(empty($albumExists[0])){
			$this->session->set_flashdata('message', 'Invalid album information');
			redirect(base_url() . 'artist/index/view'); 
		}
		
		$message =	'Update failed please try again';
		/** check mandatory fields*/
		if (!empty($_POST['email']) && !empty($_POST['username']) && !empty($_POST['artistType']) ){
			$email			= $_POST['email'];
			$oldEmail		= (!empty($_POST['old_email'])?$_POST['old_email']:'');
			$username		= $_POST['username'];
			$olduUsername	= (!empty($_POST['old_username'])?$_POST['old_username']:'');
				
			$userExists		= '';
			$usernameExists	= '';
			/** old email and new email are same then no need to check email exists or not */
			if($email	!= $oldEmail){
				$userExists	=	$this->Artist_model->artistDetail( $email );
			}
			
			/** old username and new username are same then no need to check username exists or not */
			if($username	!= $olduUsername){
				$usernameExists	=	$this->Artist_model->artistDetail( '', $username );
			}
			if ( empty( $usernameExists )) {
				if ( empty( $userExists )) {
					
					/** image upload **/
					$image = $_POST['old_image'];
					if ( isset( $_FILES['image']['error'] ) && $_FILES['image']['error'] == 0) {
						$config['upload_path'] = dirname($_SERVER["SCRIPT_FILENAME"])."/assets/upload_images/artist/";
						$config['allowed_types'] = '*';
						// 	        $config['max_size'] = '100';
						// 	        $config['max_width']  = '1024';
						// 	        $config['max_height']  = '768';
						$config['file_name']  = 'profile_'.strtotime(date(DATE_TIME_FORMAT));
						$this->load->library('upload', $config);
						if ( ! $this->upload->do_upload('image')){
							$message =  $this->upload->display_errors();
							/** failure activity entry start **/
							$activity = array(
									'action'	=>	ACTION_UPDATE,
									'status' 	=>	ACTIVITY_STATUS_FAILURE,
									'input'		=>	json_encode($_FILES),
									'activity' 	=>	$class.' '.$message
							);
							$insertData	=	$this->Event_model->eventTracking( $activity );
							/** failure activity entry end **/
						}else{
							$data = array('upload_data' => $this->upload->data());
						}
						$image = (!empty($data['upload_data']['file_name'])?$data['upload_data']['file_name']:'');
					}
						
					$updateArtist 	= '';
					/** get input modified data list */
					$artistInfo		= $this->Artist_model->setUpdateInfo($_POST);
					
					$created	=	date(DATE_TIME_FORMAT);
					$artistInfo['ARTISTS_UPDATED_ON']		= $created;
					$artistInfo['ARTISTS_IMAGE']			= $image;
					
					if(!empty( $artistInfo )){
						$updateArtist	    =   $this->Artist_model->updateArtist($artistInfo, $artistId);
					}
					
					if (!empty( $updateArtist )) {
						$message = "Update Success";
						$flag =1;
					}else{
						$message = "Update Failed";
					}
				}else{
					$message = 'User Already exists with same email';
				}
			}else{
				$message = 'User Already exists with same username';
			}
		}
		
		/** activity entry start **/
		if ( $flag ){
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_SUCCESS,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass = "success";
		}else{
			$activity = array(
					'action'	=>	ACTION_UPDATE,
					'status' 	=>	ACTIVITY_STATUS_FAILURE,
					'input'		=>	json_encode($_POST),
					'activity' 	=>	$class.' '.$message
			);
			$flashClass = "failure";
		}
		$insertData	=	$this->Event_model->eventTracking( $activity );
		/** activity entry end **/
		
// 		$this->session->set_flashdata('message', $message);
		$this->session->set_flashdata('item', array('message' => $message,'class' => $flashClass));
		redirect(base_url() . 'artist/index/edit_artist/'.$artistId);
	}
	
	/**
	 * Artist information view page
	 * artistId based on information viewed
	 * @param unknown $artistId
	 */
	public function information( $artistId ){
		/** checking input empty or not if empty then redirect view page**/
		if (empty($artistId)){
			redirect(base_url().'artist/index/view');
		}
		
		$this->data ='';
		/** get artist type list */
		$type	=	$this->Common_model->getArtistType( );
		$atrist = array();
		if(!empty($type)){
			$atrist['']='Select type';
			foreach ($type as $val){
				$atrist[$val['ARTISTS_TYPE_ID']]=$val['ARTISTS_TYPE_NAME'];
			}
			$this->data['artistType'] = $atrist;
		}
		
	
		/** get artist details based on artistId */
		$artistDetails	=	$this->Artist_model->getArtistDetail( '','', $artistId );
		if (!empty( $artistDetails[0])){
			$this->data['artistData'] = $artistDetails[0];
		}
	
		$this->load->view('layout/header');
		$this->load->view('artist/info',$this->data);
		$this->load->view('layout/footer');
	}
 
}
 
?>