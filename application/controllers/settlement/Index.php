<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Index extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	function index() {
		$this->load->view('layout/header');
		$this->load->view('settlement/index');
		$this->load->view('layout/footer');
	}
	
	 
}
 
?>