<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
                                          
class Subscription extends CI_Controller {
 
	function __construct(){	
		parent::__construct();
		$this->load->helper(array('url', 'language'));	
		$this->load->helper('form');
		$this->load->library('session');
		if( !isset($this->session->userdata['ADMIN_ID']) || empty($this->session->userdata['ADMIN_ID'] )){
			redirect(base_url());
		}
	}
 
	function index() {
		$this->load->view('layout/header');
		$this->load->view('report/subscription');
		$this->load->view('layout/footer');
	}
	
	 
}
 
?>