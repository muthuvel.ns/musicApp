<?php

Class Manager_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /** 
     * get count artist,song,album and user
     * @return array
     */
	function manageDashboardInfo(  ) {
		$this->db->select('count(*) as ALBUM_COUNT');
		$query = $this->db->get('albums');
		$result['album']=  $query->row_array();
		
		$this->db->select('count(*) as ARTIST_COUNT');
		$query = $this->db->get('artists');
		$result['artist']=  $query->row_array();
		
		$this->db->select('count(*) as SONGS_COUNT');
		$query = $this->db->get('songs');
		$result['songs']=  $query->row_array();
		
		$this->db->select('count(*) as USER_COUNT');
		$query = $this->db->get('users');
		$result['user']=  $query->row_array();
		
		return $result;
	}
	
	/**
	 * Searching song information based on request data
	 * @param array $request
	 * @return array
	 */
	function manageSongInfo( ) {
		$respose = array();

		$this->db->select('song.SONG_ID,song.SONG_NAME,song.SONG_LISTENED_COUNT')
					->select('album.ALBUM_ID, album.ALBUM_NAME, art.ARTISTS_USERNAME')
					->join('songs as song','sh.SONG_ID=song.SONG_ID')
					->join('albums as album','sh.ALBUM_ID=album.ALBUM_ID')
					->join('artists as art','album.ALBUM_ARTISTS_ID=art.ARTISTS_ID','left')
					->group_by('sh.SONG_ID');
	
		$this->db->limit( 500 );
		$query = $this->db->get('song_history As sh');
	
		// echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function manageDashboardSearchInfo( $start=0, $end=1000 ) {
		$respose = array();
		
		if (!empty($_POST['order'][0])){
			$requestOrder = $this->ordering( $_POST['order'][0] );
		}
			
		$this->db->select('song.SONG_ID,song.SONG_NAME,song.SONG_LISTENED_COUNT')
					->select('album.ALBUM_ID, album.ALBUM_NAME, art.ARTISTS_USERNAME')
					->join('songs as song','sh.SONG_ID=song.SONG_ID')
					->join('albums as album','sh.ALBUM_ID=album.ALBUM_ID')
					->join('artists as art','album.ALBUM_ARTISTS_ID=art.ARTISTS_ID','left')
					->group_by('sh.SONG_ID');

		if (!empty( $requestOrder )){
			$this->db->order_by( $requestOrder['name'], $requestOrder['order']);
		}
			
		$this->db->limit( $end, $start);
		$query = $this->db->get('song_history as sh');
		return $query->result_array();
	}
	
	function ordering($orderData){
		if (empty($orderData)){
			return array();
		}
		$info = array();
		$info['name']='sh.SONG_ID';
		if ($orderData['column'] ==0 ){
			$info['name'] = 'album.ALBUM_NAME';
		}elseif ($orderData['column'] ==1 ){
			$info['name'] = 'song.SONG_NAME';
		}elseif ($orderData['column'] ==2 ){
			$info['name']='art.ARTISTS_USERNAME';
		}elseif ($orderData['column'] ==3 ){
			$info['name']='song.SONG_LISTENED_COUNT';
		}
	
		$info['order'] = 'DESC';
		if (!empty($orderData['dir'])){
			$info['order'] = $orderData['dir'];
		}
		return $info;
	}
}

?>
