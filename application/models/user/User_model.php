<?php

Class User_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

	function userDetail( $email='', $uname='', $userId='' ) {
		 $this->db->select('*');
	 	if (!empty($email)){
	 		$this->db->where('user.USER_EMAIL', $email);
	 	}
	 	if (!empty($uname)){
	 		$this->db->where('user.USER_USERNAME', $uname);
	 	}
	 	if (!empty($userId)){
	 		$this->db->where('user.USER_ID', $userId);
	 	}
		
		$query = $this->db->get('users as user');

        return $query->result_array();
	}
   
	public function insertUser($data){
		if(empty($data)){
			return false;
		}
		
		$result = $this->db->insert('users',$data);
		
		return $this->db->insert_id();
	}

	public function updateUser($data, $id){
		if(empty($data) || empty($id)){
			return false;
		}
		
					$this->db->where('USER_ID', $id);
		$result = $this->db->update('users', $data); 
		return $result;
	}
	
	public function setUpdateInfo( $request ){
		$info = array();
		if(empty( $request )){
			return $info;
		}
		
		if($request['email']!= $request['old_email'])
			$info['USER_EMAIL'] = $request['email'];
		if(!empty($request['password'])){
			if(md5($request['password'])!= $request['old_password'])
				$info['USER_PASSWORD'] = md4($request['password']);
		}
		if($request['username']!= $request['old_username'])
			$info['USER_USERNAME'] = $request['username'];
		if($request['gender']!= $request['old_gender'])
			$info['USER_GENDER'] = $request['gender'];
		if($request['mobile']!= $request['old_mobile'])
			$info['USER_PHONE'] = $request['mobile'];
		if($request['user_status']!= $request['old_status'])
			$info['USER_STATUS'] = $request['user_status'];
		if($request['dob']!= $request['old_dob'])
			$info['USER_DOB'] = date(DATE_FORMAT,strtotime($request['dob']));
// echo '<pre>';print_r($info);exit;			
		return $info;
	}
   
}

?>
