<?php

Class Manager_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searching user information based on request data
     * @param array $request
     * @return array
     */
	function manageUserSearchInfo( $request, $start=0, $end=1000  ) {
		$respose = array();
		 if(empty($request)){
		 	return $respose;
		 }
		 
		 if (!empty($_POST['order'][0])){
		 	$requestOrder = $this->userOrdering( $_POST['order'][0] );
		 }

		 $userId	= (!empty($request['userId'])?$request['userId']:'');
		 $username	= (!empty($request['username'])?$request['username']:'');
		 $email		= (!empty($request['email'])?$request['email']:'');
		 $gender	= (!empty($request['gender'])?$request['gender']:'');
		 $startDate = (!empty($request['START_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['START_DATE_TIME'])):'');
		 $endDate	= (!empty($request['END_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['END_DATE_TIME'])):'');

		 $this->db->select('user.USER_ID,user.USER_USERNAME,USER_EMAIL,user.USER_DOB,user.USER_GENDER,user.USER_PHONE,user.USER_STATUS');
		 
		 if(!empty( $userId )){
		 	$this->db->where('user.USER_ID', $userId);
		 }
		 
		if(!empty( $email )){
			$this->db->like('user.USER_EMAIL', $email);
		}
		if (!empty( $username)) {
			$this->db->like('user.USER_USERNAME', $username);
		}
		if(!empty( $gender )){
			$this->db->where('user.USER_GENDER', $gender);
		}
		if(!empty( $startDate )){
			$this->db->where('user.USER_CREATED_ON >=',$startDate);
		}
		if(!empty( $endDate )){
			$this->db->where('user.USER_UPDATED_ON <=',$endDate);
		}
		if (!empty( $requestOrder )){
			$this->db->order_by( $requestOrder['name'], $requestOrder['order']);
		}
		$this->db->limit( $end, $start);
		$query = $this->db->get('users as user');
// 		echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function userOrdering($orderData){
		if (empty($orderData)){
			return array();
		}
		$info = array();
		$info['name']='user.USER_ID';
		if ($orderData['column'] ==0 ){
			$info['name'] = 'user.USER_USERNAME';
		}elseif ($orderData['column'] ==1 ){
			$info['name'] = 'user.USER_EMAIL';
		}elseif ($orderData['column'] ==2 ){
			$info['name']='user.USER_DOB';
		}elseif ($orderData['column'] ==3 ){
			$info['name']='user.USER_GENDER';
		}elseif ($orderData['column'] ==4 ){
			$info['name']='user.USER_STATUS';
		}
		
		$info['order'] = 'DESC';
		if (!empty($orderData['dir'])){
			$info['order'] = $orderData['dir'];
		}
		return $info;
	}
}

?>
