<?php

Class Event_model extends CI_Model {

    function __construct() {
        parent::__construct();
	}

   public function eventTracking( $data){
   		$result=0;
   		if ( empty( $data )) {
   			return $result;
   		}
   		
   		$userId 	= (!empty($data['userid'])?$data['userid']:'');
   		$action 	= (!empty($data['action'])?$data['action']:'');
   		$jsonData 	= (!empty($data['input'])?$data['input']:'');
   		$activity 	= (!empty($data['activity'])?$data['activity']:'');
   		$staus	 	= (!empty($data['status'])?$data['status']:'');
   	
   		$loginDate	=	date(DATE_TIME_FORMAT);
   		$activitydata	= array(
				   				'ACTIVITY_ACTION'	=> $action,
				   				'ACTIVITY_DATA'		=> $jsonData,
   								'ACTIVITY_DATA1'	=> $activity,
   								'ACTIVITY_DATE'		=> $loginDate,
				   				'SYSTEM_IP' 		=> $this->getRealIpAddr(),
				   				'SYSTEM_INFO' 		=> json_encode($_SERVER),
				   				'STATUS' 			=> $staus,
				   				'ACTIVITY_BY' 		=> $userId,
				   		);
   		$insert	=	$this->db->insert('activity_tracking', $activitydata);
   		return $insert;
   }
   
   function getRealIpAddr()  {
   	if (!empty($_SERVER['HTTP_CLIENT_IP'])){   //check ip from share internet
   		$ip=$_SERVER['HTTP_CLIENT_IP'];
   	}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){ //to check ip is pass from proxy
   		$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
   	}else{
   		$ip=$_SERVER['REMOTE_ADDR'];
   	}
   	return $ip;
   }
   
   
}

?>
