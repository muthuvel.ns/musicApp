<?php

Class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
		$this->load->database();
    }

    /**
     * To validate username and password
     * @param array $options
     * @return array
     */
    function loginValidation( $options ) {
		/** variable declarion*/
        $userDetail = array();
		$username = (!empty($options['username'])) ? $this->specialCharacterRemoval($options['username']) : '';
		$password = (!empty($options['password'])) ? $options['password'] : '';

		/** validate empty or not */
        if (empty($username) || empty($password))
            return $userDetail;
		
		/** convert password md5 format*/
        $pass = md5($password);
		
		/** check login credential using username*/
        $userDetail = $this->getLoginUserCredentialDetail($username);
		
		/** validate user given password vs DB password */
        if (!empty($userDetail) && $pass) {
            if ($userDetail['0']['PASSWORD'] != $pass) {
                $userDetail = array();
            }
        }
        return $userDetail;
    }

    /**
     * get login user information based on username
     * @param string $username
     * @return unknown
     */
    function getLoginUserCredentialDetail( $username ) {
        $result = array();
        if (empty($username)) {
            return $result;
        }

		$this->db->select('user.ADMIN_USER_ID,user.PASSWORD,user.FIRSTNAME,user.LASTNAME,user.EMAIL')
				->where('user.USERNAME',$username)
				->where('user.ACCOUNT_STATUS',1);
		$query = $this->db->get('admin_user as user');
        
 //      $query = 'SELECT * FROM admin_user as user WHERE user.USERNAME = '.$this->db->escape($username);
 //       $sql = "SELECT * FROM admin_user as user WHERE user.USERNAME  = ? ";
 //       $query = $this->db->query($sql, $this->db->escape($username));
 //       $this->db->query($query);
		
//		echo $this->db->last_query();exit;
		//echo '<pre>';print_r($query->result_array());exit;
        return $query->result_array();
    }
	
    /**
     * remove special charactors
     * @param string $string
     * @return mixed
     */
	 function specialCharacterRemoval( $string ) {
       return preg_replace('#[^\w()/.%\-]#'," ", $string);
    }

   
}

?>
