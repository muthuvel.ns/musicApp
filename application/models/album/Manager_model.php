<?php

Class Manager_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searching album information based on request data
     * @param array $request
     * @return array
     */
	function manageAlbumSearchInfo( $request, $start=0, $end=1000 ) {
		$respose = array();
		 if(empty($request)){
		 	return $respose;
		 }
		 
		 if (!empty($_POST['order'][0])){
		 	$requestOrder = $this->albumOrdering( $_POST['order'][0] );
		 }

		 $albumname = (!empty($request['albumname'])?$request['albumname']:'');
		 $artistId	= (!empty($request['artistId'])?$request['artistId']:'');
		// $genresType = (!empty($request['genresType'])?$request['genresType']:'');
		// $moodType	= (!empty($request['moodType'])?$request['moodType']:'');
		 $startDate = (!empty($request['START_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['START_DATE_TIME'])):'');
		 $endDate	= (!empty($request['END_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['END_DATE_TIME'])):'');

		 $this->db->select('album.ALBUM_ID, album.ALBUM_NAME,album.ALBUM_LOGO,album.ALBUM_ARTISTS_ID,album.ALBUM_GENRES_ID,album.ALBUM_MOOD_ID,album.ALBUM_LOCATION,album.ALBUM_CITY,album.ALBUM_COUNTRY,album.ALBUM_IS_PODCAST, album.ALUBM_PODCAST_CATEGORY, album.ALBUM_STATUS')
		 			->select('art.ARTISTS_USERNAME, COUNT(song.SONG_ALBUM_ID) AS SONG_COUNT')
// 		 			->select('art.ARTISTS_USERNAME, gen.GENRES_NAME, mood.MOOD_NAME')
		 			->join('songs as song','song.SONG_ALBUM_ID=album.ALBUM_ID','left')
		 			->join('artists as art','art.ARTISTS_ID=album.ALBUM_ARTISTS_ID','left');
// 		 			->join('genres_types as gen','gen.GENRES_ID=album.ALBUM_GENRES_ID','left')
// 		 			->join('mood_types as mood','mood.MOOD_ID=album.ALBUM_MOOD_ID','left');

		 if(!empty( $albumname )){
		 	$this->db->like('album.ALBUM_NAME', $albumname);
		 }
// 		 if(!empty( $albumId )){
// 		 	$this->db->where('album.ALBUM_ID', $albumId);
// 		 }
		 if (!empty( $artistId)) {
			 $this->db->where('album.ALBUM_ARTISTS_ID', $artistId);
		 }
// 		 if(!empty( $genresType )){
// 		 	$this->db->where('album.ALBUM_GENRES_ID', $genresType);
// 		 }
// 		 if(!empty( $moodType )){
// 		 	$this->db->where('album.ALBUM_MOOD_ID', $moodType);
// 		 }
		 if(!empty( $startDate )){
			$this->db->where('album.ALBUM_CREATED_ON >=',$startDate);
		}
		if(!empty( $endDate )){
			$this->db->where('album.ALBUM_UPDATED_ON <=',$endDate);
		}
		
		$this->db->group_by('album.ALBUM_ID');
		
		if (!empty( $requestOrder )){
			$this->db->order_by( $requestOrder['name'], $requestOrder['order']);
		}
		
		$this->db->limit( $end, $start);
		$query = $this->db->get('albums As album');
      
// 		echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function albumOrdering($orderData){
		if (empty($orderData)){
			return array();
		}
		$info = array();
		$info['name']='album.ALBUM_ID';
		if ($orderData['column'] ==0 ){
			$info['name'] = 'album.ALBUM_NAME';
		}elseif ($orderData['column'] ==1 ){
			$info['name'] = 'SONG_COUNT';
		}elseif ($orderData['column'] ==2 ){
			$info['name']='album.ALBUM_LOGO';
		}elseif ($orderData['column'] ==3 ){
			$info['name']='album.ALBUM_STATUS';
		}
	
		$info['order'] = 'DESC';
		if (!empty($orderData['dir'])){
			$info['order'] = $orderData['dir'];
		}
		return $info;
	}
}

?>
