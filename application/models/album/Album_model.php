<?php

Class Album_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * get album details based on @param
     * @param string $name
     * @param string $albumId
     * @param string $artistId
     * @return array
     */
	function albumDetail($name='', $albumId='', $artistId='' ) {
		 $this->db->select('ALBUM_ID, ALBUM_NAME,ALBUM_LOGO,ALBUM_ARTISTS_ID,ALBUM_GENRES_ID,ALBUM_MOOD_ID,ALBUM_LOCATION,ALBUM_CITY,ALBUM_COUNTRY,ALBUM_IS_PODCAST, ALUBM_PODCAST_CATEGORY, ALBUM_STATUS');
		 if(!empty( $name )){
		 	$this->db->where('ALBUM_NAME', $name);
		 }
		 if(!empty( $albumId )){
		 	$this->db->where('ALBUM_ID', $albumId);
		 }
		 if (!empty( $artistId)) {
			 $this->db->where('ALBUM_ARTISTS_ID', $artistId);
		 }
		$query = $this->db->get('albums');
        return $query->result_array();
	}
   
	/**
	 * Insert Album details based on input array
	 * @param array $data
	 * @return boolean|unknown
	 */
  	public function insertAlbum($data){
		if(empty($data)){
			return false;
		}
		
		$result = $this->db->insert('albums',$data);
		
		return $this->db->insert_id();
	}

	/**
	 * Update Album details based on input array
	 * @param array $data
	 * @param integer $id
	 * @return boolean|unknown
	 */
	public function updateAlbum($data, $id){
		if(empty($data) || empty($id)){
			return false;
		}
		
					$this->db->where('ALBUM_ID', $id);
		$result = $this->db->update('albums', $data); 
		return $result;
	}
	
	/**
	 * get alubum , artist name and over all inforamtion 
	 * @param string $name
	 * @param string $albumId
	 * @param string $artistId
	 * @return array
	 */
	function getAlbumDetail($name='', $albumId='', $artistId='' ) {
		$this->db->select('al.ALBUM_ID, al.ALBUM_NAME,al.ALBUM_LOGO,al.ALBUM_ARTISTS_ID,al.ALBUM_GENRES_ID,al.ALBUM_MOOD_ID,al.ALBUM_LOCATION,al.ALBUM_CITY,al.ALBUM_COUNTRY,al.ALBUM_IS_PODCAST, al.ALUBM_PODCAST_CATEGORY, al.ALBUM_STATUS')
				->select('art.ARTISTS_USERNAME, gen.GENRES_NAME,mood.MOOD_NAME,podcast.PODCAST_NAME')
				->join('artists as art','art.ARTISTS_ID=al.ALBUM_ARTISTS_ID','left')
				->join('genres_types as gen','gen.GENRES_ID=al.ALBUM_GENRES_ID','left')
				->join('mood_types as mood','mood.MOOD_ID=al.ALBUM_MOOD_ID','left')
				->join('podcast_categories as podcast','podcast.PODCAST_ID=al.ALUBM_PODCAST_CATEGORY','left');
		if(!empty( $name )){
			$this->db->where('al.ALBUM_NAME', $name);
		}
		if(!empty( $albumId )){
			$this->db->where('al.ALBUM_ID', $albumId);
		}
		if (!empty( $artistId)) {
			$this->db->where('al.ALBUM_ARTISTS_ID', $artistId);
		}
		$query = $this->db->get('albums as al');
		return $query->result_array();
	}
	
	/**
	 * To set update information
	 * Here checking old and new data any changes their add to array
	 * otherwise not added in the array
	 * @param unknown $request
	 * @return array modified info
	 */
	public function setUpdateInfo( $request ){
		$info = array();
		if(empty( $request )){
			return $info;
		}
		
		if($request['album_name']	!= $request['old_name'])
			$info['ALBUM_NAME']	= $request['album_name'];
		if($request['artistId']	!= $request['old_artistId'])
			$info['ALBUM_ARTISTS_ID'] = $request['artistId'];
		if($request['genresType']!= $request['old_genresType'])
			$info['ALBUM_GENRES_ID'] = $request['genresType'];
		if($request['moodType']!= $request['old_moodType'])
			$info['ALBUM_MOOD_ID'] = $request['moodType'];
		if($request['country']!= $request['old_country'])
			$info['ALBUM_COUNTRY'] = $request['country'];
		if($request['location']!= $request['old_location'])
			$info['ALBUM_LOCATION'] = $request['location'];
		if($request['city']!= $request['old_city'])
			$info['ALBUM_CITY'] = $request['city'];
		if($request['album_status']!= $request['old_status'])
			$info['ALBUM_STATUS'] = $request['album_status'];
		/*if(isset($request['podcast'])){
			$info['ALBUM_IS_PODCAST'] = 1;
			$info['ALUBM_PODCAST_CATEGORY'] = $request['podcastType'];
		}*/
		/*if($request['image']!= $request['old_image'])
			$info['image'] = $request['image'];*/
		return $info;
	}
   
}

?>
