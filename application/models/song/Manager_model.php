<?php

Class Manager_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searching song information based on request data
     * @param array $request
     * @return array
     */
	function manageSongSearchInfo( $request, $start=0, $end=1000 ) {
		$respose = array();
		 if(empty($request)){
		 	return $respose;
		 }

		 if (!empty($_POST['order'][0])){
		 	$requestOrder = $this->songOrdering( $_POST['order'][0] );
		 }
		 
		 $songId		= (!empty($request['songId'])?$request['songId']:'');
		 $songname		= (!empty($request['songname'])?$request['songname']:'');
		 $song_amt		= (!empty($request['song_amt'])?$request['song_amt']:'');
		 $albumId		= (!empty($request['albumId'])?$request['albumId']:'');
		 $genresType	= (!empty($request['genresType'])?$request['genresType']:'');
		 $moodType		= (!empty($request['moodType'])?$request['moodType']:'');
		 $startDate		= (!empty($request['START_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['START_DATE_TIME'])):'');
		 $endDate		= (!empty($request['END_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['END_DATE_TIME'])):'');

		 $this->db->select('song.SONG_ID,song.SONG_ALBUM_ID,song.SONG_NAME,song.SONG_AMOUNT,song.SONG_COMPANY_PERCENTAGE,song.SONG_COVER_IMAGE,song.SONG_GENRES_ID,song.SONG_MOOD_ID')
		 			->select('song.SONG_LOCATION, song.SONG_CITY, song.SONG_COUNTRY, song.SONG_LISTENED_COUNT, song.SONG_FOLLOWER_COUNT, song.SONG_IS_PODCAST, song.SONG_PODCAST_CATEGORY, song.SONG_STATUS')
		 			->select('album.ALBUM_NAME, gen.GENRES_NAME, mood.MOOD_NAME')
		 			->join('albums as album','album.ALBUM_ID=song.SONG_ALBUM_ID','left')
		 			->join('genres_types as gen','gen.GENRES_ID=song.SONG_GENRES_ID','left')
		 			->join('mood_types as mood','mood.MOOD_ID=song.SONG_MOOD_ID','left');

 		if(!empty( $songId )){
 			$this->db->where('song.SONG_ID', $songId);
 		}
		 if(!empty( $songname )){
		 	$this->db->like('song.SONG_NAME', $songname);
		 }
		 if(!empty( $albumId )){
		 	$this->db->where('song.SONG_ALBUM_ID', $albumId);
		 }
		 if (!empty( $song_amt)) {
			 $this->db->where('song.SONG_AMOUNT', $song_amt);
		 }
		 if(!empty( $genresType )){
		 	$this->db->where('song.SONG_GENRES_ID', $genresType);
		 }
		 if(!empty( $moodType )){
		 	$this->db->where('song.SONG_MOOD_ID', $moodType);
		 }
		 if(!empty( $startDate )){
			$this->db->where('song.SONG_CREATED_ON >=',$startDate);
		}
		if(!empty( $endDate )){
			$this->db->where('song.SONG_UPDATED_ON <=',$endDate);
		}
		if (!empty( $requestOrder )){
			$this->db->order_by( $requestOrder['name'], $requestOrder['order']);
		}
		
		$this->db->limit( $end, $start);
		$query = $this->db->get('songs As song');
      
		// echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function songOrdering($orderData){
		if (empty($orderData)){
			return array();
		}
		$info = array();
		$info['name']='song.SONG_ID';
		if ($orderData['column'] ==0 ){
			$info['name'] = 'song.SONG_NAME';
		}elseif ($orderData['column'] ==1 ){
			$info['name'] = 'song.SONG_ALBUM_ID';
		}elseif ($orderData['column'] ==2 ){
			$info['name']='song.SONG_AMOUNT';
		}elseif ($orderData['column'] ==3 ){
			$info['name']='song.SONG_COMPANY_PERCENTAGE';
		}
	
		$info['order'] = 'DESC';
		if (!empty($orderData['dir'])){
			$info['order'] = $orderData['dir'];
		}
		return $info;
	}
	
}

?>
