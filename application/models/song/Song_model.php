<?php

Class Song_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * get song details based on inputs
     * @param string $name
     * @param string $songId
     * @param string $albumId
     * @return array
     */
	function songDetail($name='', $songId='', $albumId='' ) {
		 $this->db->select('SONG_ID, SONG_ALBUM_ID,SONG_NAME,SONG_URL,SONG_AMOUNT,SONG_COMPANY_PERCENTAGE,SONG_COVER_IMAGE,SONG_GENRES_ID,SONG_MOOD_ID,SONG_LOCATION,SONG_CITY, SONG_COUNTRY, SONG_IS_PODCAST,SONG_PODCAST_CATEGORY,SONG_STATUS');
		 if(!empty( $name )){
		 	$this->db->where('SONG_NAME', $name);
		 }
		 if(!empty( $songId )){
		 	$this->db->where('SONG_ID', $songId);
		 }
		 
		 if(!empty( $albumId )){
		 	$this->db->where('SONG_ALBUM_ID', $albumId);
		 }
		 
		$query = $this->db->get('songs');
        return $query->result_array();
	}
   
	/**
	 * Song insertion
	 * @param array $data
	 * @return boolean|unknown
	 */
  	public function insertSong($data){
		if(empty($data)){
			return false;
		}
		
		$result = $this->db->insert('songs',$data);
		
		return $this->db->insert_id();
	}
	/**
	 * Song updation
	 * @param array $data
	 * @param string $id
	 * @return boolean|unknown
	 */
	public function updateSong($data, $id){
		if(empty($data) || empty($id)){
			return false;
		}
		
					$this->db->where('SONG_ID', $id);
		$result = $this->db->update('songs', $data); 
		return $result;
	}
	
	/**
	 * Insert multiple records in song percentage table
	 * @param array $data
	 * @return boolean|unknown
	 */
	public function insertSongPercentage($data){
		if(empty($data)){
			return false;
		}
	
		$result = $this->db->insert_batch('song_percentages',$data);
	
		return $result;
	}
	
	/**
	 * Song percentage updation
	 * @param array $data
	 * @param string $id
	 * @return boolean|unknown
	 */
	public function updateSongPercentage( $data, $id ){
		if(empty($data)|| empty($id)){
			return false;
		}
		
		$this->db->where('PERCENTAGE_ID', $id);
		$result = $this->db->update('song_percentages', $data);
		
		return $result;
	}
	
	/* public function updateSongPercentage( $data ){
		if(empty($data)){
			return false;
		}
		//echo '<pre>';print_r($data);exit;
		$result = $this->db->update_batch('song_percentages', $data,'PERCENTAGE_ID');
	
		return $result;
	} */
	
	/* public function replacePercentage($data){
		
		if(empty($data)){
			return false;
		}
		
		return $this->db->replace('song_percentages', $data);
	} */
		
/*	function getAlbumDetail( $email='', $username='' ) {
		$this->db->select('art.ARTISTS_ID,art.ARTISTS_USERNAME,ARTISTS_EMAIL,art.ARTISTS_TYPE_ID,art.ARTISTS_PHONE,art.ARTISTS_IMAGE,art.ARTISTS_FOLLOWER_COUNT')
				->select('type.ARTISTS_TYPE_NAME')
				->join('artists_type as type','type.ARTISTS_TYPE_ID=art.ARTISTS_TYPE_ID');
		if(!empty( $email )){
			$this->db->where('ARTISTS_EMAIL', $email);
		}
		if (!empty( $username)) {
			$this->db->where('ARTISTS_USERNAME', $username);
		}
		$query = $this->db->get('albums as art');
		return $query->result_array();
	}*/
	
	/**
	 * To set update information
	 * Here checking old and new data any changes their add to array
	 * otherwise not added in the array
	 * @param unknown $request
	 * @return array modified info
	 */
	public function setUpdateInfo( $request ){
		$info = array();
		if(empty( $request )){
			return $info;
		}
		
		if($request['song_name']	!= $request['old_name'])
			$info['SONG_NAME']	= $request['song_name'];
		if($request['albumId']	!= $request['old_albumId'])
			$info['SONG_ALBUM_ID'] = $request['albumId'];
		if($request['song_amt']!= $request['old_amt'])
			$info['SONG_AMOUNT'] = $request['song_amt'];
		if($request['percentage']!= $request['old_percentage'])
			$info['SONG_COMPANY_PERCENTAGE'] = $request['percentage'];
		if($request['genresType']!= $request['old_genresType'])
			$info['SONG_GENRES_ID'] = $request['genresType'];
		if($request['moodType']!= $request['old_moodType'])
			$info['SONG_MOOD_ID'] = $request['moodType'];
		if($request['country']!= $request['old_country'])
			$info['SONG_COUNTRY'] = $request['country'];
		if($request['location']!= $request['old_location'])
			$info['SONG_LOCATION'] = $request['location'];
		if($request['city']!= $request['old_city'])
			$info['SONG_CITY'] = $request['city'];
		if($request['song_status']!= $request['old_status'])
			$info['SONG_STATUS'] = $request['song_status'];
		/*if(isset($request['podcast'])){
			$info['ALBUM_IS_PODCAST'] = 1;
			$info['ALUBM_PODCAST_CATEGORY'] = $request['podcastType'];
		}*/
		/*if($request['image']!= $request['old_image'])
			$info['image'] = $request['image'];*/
		return $info;
	}
	
	function songPercentageDetail( $perId='', $songId='', $ArtistType='') {
		$this->db->select('*');
		if(!empty( $perId )){
			$this->db->where('PERCENTAGE_ID', $perId);
		}
		if(!empty( $songId )){
			$this->db->where('SONG_ID', $songId);
		}
		
		if(!empty( $ArtistType )){
			$this->db->where('ARTIST_TYPE_ID', $ArtistType);
		}
			
		$query = $this->db->get('song_percentages');
		return $query->result_array();
	}
   
}

?>
