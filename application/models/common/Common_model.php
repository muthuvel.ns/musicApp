<?php

Class Common_model extends CI_Model {

    function __construct() {
        parent::__construct();
		$this->load->database();
    }

    /**
     * Removing special charaction given string
     * except(),%,/ and some
     * @param unknown $string
     * @return mixed
     */
    function specialCharacterRemoval( $string ) {
       return preg_replace('#[^\w()/.%\-]#'," ", $string);
    }
    
    /**
     * get artist type
     * @param string $id
     * @return array
     */
    function getArtistType($id=''){
    	$this->db->select('ARTISTS_TYPE_ID,ARTISTS_TYPE_NAME');
   		$this->db->where('ARTISTS_TYPE_STATUS', 1);
    	if (!empty( $id)) {
    		$this->db->where('ARTISTS_TYPE_ID', $id);
    	}
    	$query = $this->db->get('artists_type');
    	return $query->result_array();
    }
    /**
     * get Genres type
     * @param string $id
     * @return array
     */
    function getGenresType($id=''){
    	$this->db->select('GENRES_ID,GENRES_NAME');
    	$this->db->where('GENRES_STATUS', 1);
    	if (!empty( $id)) {
    		$this->db->where('GENRES_ID', $id);
    	}
    	$query = $this->db->get('genres_types');
    	return $query->result_array();
    }
    /**
     * get mood type
     * @param string $id
     * @return array
     */
    function getMoodType($id=''){
    	$this->db->select('MOOD_ID,MOOD_NAME');
    	$this->db->where('MOOD_STATUS', 1);
    	if (!empty( $id)) {
    		$this->db->where('MOOD_ID', $id);
    	}
    	$query = $this->db->get('mood_types');
    	return $query->result_array();
    }
    
    function getPodcastType($id=''){
    	$this->db->select('PODCAST_ID,PODCAST_NAME');
    	$this->db->where('PODCAST_STATUS', 1);
    	if (!empty( $id)) {
    		$this->db->where('PODCAST_ID', $id);
    	}
    	$query = $this->db->get('podcast_categories');
    	return $query->result_array();
    }
   
}

?>
