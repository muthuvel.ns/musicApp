<?php

Class Manager_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Searching Artist information based on request data
     * @param array $request
     * @return array
     */
	function manageArtistSearchInfo( $request, $start=0, $end=1000 ) {
		$respose = array();
		 if(empty($request)){
		 	return $respose;
		 }
		
		 if (!empty($_POST['order'][0])){
		 	$requestOrder = $this->artistOrdering( $_POST['order'][0] );
		 }
		 
		 $username	= (!empty($request['username'])?$request['username']:'');
		 $email		= (!empty($request['email'])?$request['email']:'');
		 $artistType= (!empty($request['artistType'])?$request['artistType']:'');
		 $startDate	= (!empty($request['START_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['START_DATE_TIME'])):'');
		 $endDate	= (!empty($request['END_DATE_TIME'])?date(DATE_TIME_FORMAT,strtotime($request['END_DATE_TIME'])):'');

		 $this->db->select('art.ARTISTS_ID,art.ARTISTS_USERNAME,art.ARTISTS_EMAIL,art.ARTISTS_TYPE_ID,art.ARTISTS_PASSWORD,art.ARTISTS_PHONE,art.ARTISTS_IMAGE,art.ARTISTS_FOLLOWER_COUNT')
				->select('art.ARTISTS_FIRSTNAME,art.ARTISTS_LASTNAME,art.ARTISTS_LOCATION,art.ARTISTS_COUNTRY,art.ARTISTS_CITY,art.ARTISTS_STATUS,type.ARTISTS_TYPE_NAME')
				->join('artists_type as type','type.ARTISTS_TYPE_ID=art.ARTISTS_TYPE_ID');
		if(!empty( $email )){
			$this->db->where('art.ARTISTS_EMAIL', $email);
		}
		if (!empty( $username)) {
			$this->db->like('art.ARTISTS_USERNAME', $username);
		}
		if(!empty( $artistType )){
			$this->db->where('art.ARTISTS_TYPE_ID', $artistType);
		}
		if(!empty( $startDate )){
			$this->db->where('art.ARTISTS_CREATED_ON >=',$startDate);
		}
		if(!empty( $endDate )){
			$this->db->where('art.ARTISTS_CREATED_ON <=',$endDate);
		}
		if (!empty( $requestOrder )){
			$this->db->order_by( $requestOrder['name'], $requestOrder['order']);
		}
			
		$this->db->limit( $end, $start);
		$query = $this->db->get('artists as art');
// 		 echo $this->db->last_query();exit;
		return $query->result_array();
	}
	
	function artistOrdering($orderData){
		if (empty($orderData)){
			return array();
		}
		$info = array();
		$info['name']='art.ARTISTS_ID';
		if ($orderData['column'] ==0 ){
			$info['name'] = 'art.ARTISTS_TYPE_ID';
		}elseif ($orderData['column'] ==1 ){
			$info['name'] = 'art.ARTISTS_USERNAME';
		}elseif ($orderData['column'] ==2 ){
			$info['name']='art.ARTISTS_EMAIL';
		}elseif ($orderData['column'] ==3 ){
			$info['name']='art.ARTISTS_PHONE';
		}
	
		$info['order'] = 'DESC';
		if (!empty($orderData['dir'])){
			$info['order'] = $orderData['dir'];
		}
		return $info;
	}
}

?>
