<?php

Class Artist_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * get artist details
     * @param string $email
     * @param string $username
     * @param string $id
     * @return array
     */
	function artistDetail( $email='', $username='', $id='') {
		 $this->db->select('ARTISTS_ID, ARTISTS_USERNAME,ARTISTS_EMAIL');
		 if(!empty( $email )){
		 	$this->db->where('ARTISTS_EMAIL', $email);
		 }
		 if (!empty( $username)) {
			 $this->db->where('ARTISTS_USERNAME', $username);
		 }
		 if(!empty( $id )){
		 	$this->db->where('ARTISTS_ID', $id);
		 }
		$query = $this->db->get('artists');
        return $query->result_array();
	}
	
   /**
    * Album insertion
    * @param array $data
    * @return boolean|unknown
    */
  	public function insertArtist($data){
		if(empty($data)){
			return false;
		}
		
		$result = $this->db->insert('artists',$data);
		
		return $this->db->insert_id();
	}

	/**
	 * Artist updation
	 * @param array $data
	 * @param integer $id
	 * @return boolean|unknown
	 */
	public function updateArtist($data, $id){
		if(empty($data) || empty($id)){
			return false;
		}
		
					$this->db->where('ARTISTS_ID', $id);
		$result = $this->db->update('artists', $data); 
		return $result;
	}
	
	/**
	 * get artist and artist type details
	 * @param string $email
	 * @param string $username
	 * @param string $id
	 * @return array
	 */
	function getArtistDetail( $email='', $username='', $id='', $type='' ) {
		$this->db->select('art.ARTISTS_ID,art.ARTISTS_USERNAME,ARTISTS_EMAIL,art.ARTISTS_TYPE_ID,art.ARTISTS_PASSWORD,art.ARTISTS_PHONE,art.ARTISTS_IMAGE,art.ARTISTS_FOLLOWER_COUNT')
				->select('art.ARTISTS_FIRSTNAME,art.ARTISTS_LASTNAME,art.ARTISTS_LOCATION,art.ARTISTS_COUNTRY,art.ARTISTS_CITY,art.ARTISTS_STATUS,type.ARTISTS_TYPE_NAME')
				->join('artists_type as type','type.ARTISTS_TYPE_ID=art.ARTISTS_TYPE_ID');
		if(!empty( $email )){
			$this->db->where('art.ARTISTS_EMAIL', $email);
		}
		if (!empty( $username)) {
			$this->db->where('art.ARTISTS_USERNAME', $username);
		}
		if(!empty( $id )){
			$this->db->where('art.ARTISTS_ID', $id);
		}
		if(!empty( $type )){
			$this->db->where('art.ARTISTS_TYPE_ID', $type);
		}
		$query = $this->db->get('artists as art');
		return $query->result_array();
	}
	
	/**
	 * To set update information
	 * Here checking old and new data any changes their add to array
	 * otherwise not added in the array
	 * @param unknown $request
	 * @return array modified info
	 */
	public function setUpdateInfo( $request ){
		$info = array();
		if(empty( $request )){
			return $info;
		}
		
		if($request['artistType']!= $request['old_artistType'])
			$info['ARTISTS_TYPE_ID'] = $request['artistType'];
		if($request['email']!= $request['old_email'])
			$info['ARTISTS_EMAIL'] = $request['email'];
		if(!empty($request['password'])){
			if(md5($request['password'])!= $request['old_password'])
				$info['ARTISTS_PASSWORD'] = md4($request['password']);
		}
		if($request['username']!= $request['old_username'])
			$info['ARTISTS_USERNAME'] = $request['username'];
		if($request['fname']!= $request['old_fname'])
			$info['ARTISTS_FIRSTNAME'] = $request['fname'];
		if($request['lname']!= $request['old_lname'])
			$info['ARTISTS_LASTNAME'] = $request['lname'];
		if($request['mobile']!= $request['old_mobile'])
			$info['ARTISTS_PHONE'] = $request['mobile'];
		if($request['country']!= $request['old_country'])
			$info['ARTISTS_COUNTRY'] = $request['country'];
		if($request['city']!= $request['old_city'])
			$info['ARTISTS_CITY'] = $request['city'];
		if($request['location']!= $request['old_location'])
			$info['ARTISTS_LOCATION'] = $request['location'];
		if($request['artist_status']!= $request['old_status'])
			$info['ARTISTS_STATUS'] = $request['artist_status'];
		/*if($request['image']!= $request['old_image'])
			$info['image'] = $request['image'];*/
		return $info;
	}
   
}

?>
